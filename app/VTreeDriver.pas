Unit VTreeDriver;

Interface

Uses
  Windows;




Type
  TDriverMajorFunctions = Packed Array [0..28] Of Pointer;

  TSnapshotType = (stDriverList, stDriverInfo, stDeviceInfo);

  TDeviceSnapshot = Record
    Name : WideString;
    Address : Pointer;
    DriverName : WideString;
    DriverAddress : Pointer;
    NumberOfLowerDevices : Integer;
    LowerDevices : Array Of Pointer;
    NumberOfUpperDevices : Integer;
    UpperDevices : Array Of Pointer;
    DisplayName : WideString;
    Description : WideString;
    Vendor : WideString;
    ClassName : WideString;
    Location : WideString;
    Enumerator : WideString;
    ClassGuid : WideString;
    Flags : Cardinal;
    Characteristics : Cardinal;
    DeviceType : Cardinal;
    DiskDeviceAddress : Pointer;
    end;
  PDeviceSnapshot = ^TDeviceSnapshot;

  TDriverSnapshot = Record
    Name : WideString;
    Address : Pointer;
    MajorFunction : TDriverMajorFunctions;
    NumberOfDevices : Integer;
    Devices : Array Of Pointer;
    end;
  PDriverSnapshot = ^TDriverSnapshot;

  TSnapshot = Record
    NumberOfDrivers : Integer;
    Drivers : Array Of Pointer;
    end;
  PSnapshot = ^TSnapshot;

  SNAPSHOT_DRIVERLIST = Record
    Size : NativeInt;
    NumberOfDrivers : NativeInt;
    DriversOffset : NativeInt;
    end;
  PSNAPSHOT_DRIVERLIST = ^SNAPSHOT_DRIVERLIST;

  SNAPSHOT_DRIVERINFO = Record
    Size : NativeInt;
    NameOffset : NativeInt;
    ObjectAddress : Pointer;
    NumberOfDevices : NativeInt;
    DevicesOffset : NativeInt;
    MajorFunction : TDriverMajorFunctions;
    end;
  PSNAPSHOT_DRIVERINFO = ^SNAPSHOT_DRIVERINFO;

  SNAPSHOT_DEVICEINFO = Record
    Size : NativeInt;
    NameOffset : NativeInt;
    ObjectAddress : Pointer;
    Flags : ULONG;
    Characteristics : ULONG;
    DeviceType : ULONG;
    NumberOfLowerDevices : NativeInt;
    LowerDevicesOffset : NativeInt;
    NumberOfUpperDevices : NativeInt;
    UpperDevicesOffset : NativeInt;
    DisplayNameOffset : NativeInt;
    VendorNameOffset : NativeInt;
    DescriptionOffset : NativeInt;
    EnumeratorOffset : NativeInt;
    LocationOffset : NativeInt;
    ClassNameOffset : NativeInt;
    ClassGuidOffset : NativeInt;
    DiskDevice : Pointer;
    end;
  PSNAPSHOT_DEVICEINFO = ^SNAPSHOT_DEVICEINFO;

Function DriverInstall:Boolean;
Procedure DriverUninstall;
Function DriverLoad:Boolean;
Procedure DriverUnload;

Function DriverCreateSnapshot(Var ASnapshot:Pointer):Boolean;
Function DriverFreeSnapshot(ASnapshot:Pointer):Boolean;
Function DriverReadSnapshot(ASnapshotType:TSnapshotType; ASnapshot:Pointer; Var AResult:Pointer):Boolean;

Implementation

Uses
  SysUtils, scmDrivers;

Const
  DriverServiceName = 'VrtuleTree';
  DriverFIleName = 'VrtuleTree.sys';
  DriverDeviceName = '\\.\VrtuleTree';

  IOCTL_VRTULETREE_CREATE_SNAPSHOT          = $228008;
  IOCTL_VRTULETREE_READ_SNAPSHOT_DRIVERLIST = $22400c;
  IOCTL_VRTULETREE_READ_SNAPSHOT_DRIVERINFO = $224010;
  IOCTL_VRTULETREE_READ_SNAPSHOT_DEVICEINFO = $224014;
  IOCTL_VRTULETREE_FREE_SNAPSHOT            = $228018;


Var
  hDevice : THandle;

Function DriverInstall:Boolean;
Var
  FullFileName : WideString;
begin
FullFileName := Format('%s%s', [ExtractFilePath(ParamStr(0)), DriverFileName]);
Result := ScmDriverInstall(DriverServiceName, FullFileName);
end;

Procedure DriverUninstall;
begin
ScmDriverUninstall(DriverServiceName);
end;

Function DriverLoad:Boolean;
begin
Result := ScmDriverLoad(DriverServiceName);
If Result Then
  begin
  hDevice := CreateFileW(DriverDeviceName, GENERIC_ALL, 0, Nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  Result := hDevice <> INVALID_HANDLE_VALUE;
  If Not Result Then
    ScmDriverUnload(DriverServiceName);
  end;
end;

Procedure DriverUnload;
begin
CloseHandle(hDevice);
ScmDriverUnload(DriverServiceName);
end;

Function DriverIOCTL(ACode:Cardinal; AInBuffer:Pointer; AInBufferLength:Integer; AOutBUffer:Pointer; AOutBufferLength:Cardinal):Boolean;
Var
  Dummy : DWORD;
begin
Result := DeviceIoControl(hDevice, ACode, AInBuffer, AInBufferLength, AOutBUffer, AOutBufferLength, Dummy, Nil);
end;

Function DriverCreateSnapshot(Var ASnapshot:Pointer):Boolean;
begin
Result := DriverIOCTL(IOCTL_VRTULETREE_CREATE_SNAPSHOT, Nil, 0, @ASnapshot, SizeOf(ASnapshot));
end;

Function DriverFreeSnapshot(ASnapshot:Pointer):Boolean;
begin
Result := DriverIOCTL(IOCTL_VRTULETREE_FREE_SNAPSHOT, @ASnapshot, SizeOf(ASnapshot), Nil, 0);
end;


Function DriverReadSnapshot(ASnapshotType:TSnapshotType; ASnapshot:Pointer; Var AResult:Pointer):Boolean;
Var
  BufSize : Integer;
  Err : LONG;
  Code : Cardinal;
begin
Result := False;
AResult := Nil;
BufSize := 512;
Case ASnapshotType Of
  stDriverList : Code := IOCTL_VRTULETREE_READ_SNAPSHOT_DRIVERLIST;
  stDriverInfo : Code := IOCTL_VRTULETREE_READ_SNAPSHOT_DRIVERINFO;
  stDeviceInfo : Code := IOCTL_VRTULETREE_READ_SNAPSHOT_DEVICEINFO;
  end;

Repeat
If Assigned(AResult) Then
  HeapFree(GetProcessHeap, 0, AResult);

BufSize := BufSize * 2;
AResult := HeapAlloc(GetProcessHeap, 8, BufSize);
If Assigned(AResult) Then
  begin
  Result := DriverIOCTL(Code, @ASnapshot, SizeOf(ASnapshot), AResult, BufSize);
  If Result Then
    Err := ERROR_SUCCESS
  Else Err := GetLastError;
  end
Else Err := ERROR_NOT_ENOUGH_MEMORY
Until Err <> ERROR_INSUFFICIENT_BUFFER;

If Err <> ERROR_SUCCESS Then
  begin
  HeapFree(GetProcessHeap, 0, AResult);
  AResult := Nil;
  end;
end;




End.
