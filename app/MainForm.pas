Unit MainForm;

Interface

Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,
  System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls,
  HashTable, Vcl.Menus, VTreeDriver, Vcl.StdCtrls, Vcl.ExtCtrls;

Type
  TDeviceNodeType = (dntLower, dntCurrent, dntUpper);

  TForm1 = Class (TForm)
    DriverDeviceTreeView: TTreeView;
    PageControl1: TPageControl;
    DeviceTabSheet: TTabSheet;
    DriverTabSheet: TTabSheet;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Createsnapshot1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    DriverDevicesListView: TListView;
    MajorFunctionListview: TListView;
    Panel1: TPanel;
    DriverAddressLEdit: TLabeledEdit;
    DriverNameLEdit: TLabeledEdit;
    Panel2: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox3: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    GroupBox4: TGroupBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    CheckBox18: TCheckBox;
    CheckBox19: TCheckBox;
    CheckBox20: TCheckBox;
    CheckBox21: TCheckBox;
    CheckBox22: TCheckBox;
    GroupBox5: TGroupBox;
    DevicePnpListView: TListView;
    CheckBox23: TCheckBox;
    CheckBox24: TCheckBox;
    CheckBox25: TCheckBox;
    CheckBox26: TCheckBox;
    CheckBox27: TCheckBox;
    CheckBox28: TCheckBox;
    CheckBox29: TCheckBox;
    CheckBox30: TCheckBox;
    CheckBox31: TCheckBox;
    CheckBox32: TCheckBox;
    Edit8: TEdit;
    Label8: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Exit1Click(Sender: TObject);
    procedure Createsnapshot1Click(Sender: TObject);
    procedure DriverDeviceTreeViewChange(Sender: TObject; Node: TTreeNode);
  Private
    FDriverTable : TAddressHashTable;
    FDeviceTable : TAddressHashTable;
    FSnapshot : PSnapshot;
    Procedure CreateSnapshot;
    Procedure DestroySnapshot;
    Procedure FreeDeviceRecord(ARecord:Pointer);
    Procedure FreeDriverRecord(ARecord:Pointer);
    Function CreateDriverNode(ADriverRecord:PDriverSnapshot):TTreeNode;
    Function CreateDeviceNode(ADeviceRecord:PDeviceSnapshot; AParent:TTreeNode; ANodeType:TDeviceNodeType):TTreeNode;
    Function CreateDeviceNodes(ADeviceRecord:PDeviceSnapshot; AParent:TTreeNode):TTreeNode;
    Procedure DisplaySnapshot;
    Procedure DisplayDriverInfo(ADriverRecord:PDriverSnapshot);
    Procedure displayDeviceInfo(ADeviceRecord:PDeviceSnapshot);

    Function CopyString(AStart:Pointer; AOffset:Cardinal):WideString;

  end;

Var
  Form1: TForm1;

Implementation

{$R *.DFM}

Uses
  Kernel;

Function TForm1.CopyString(AStart:Pointer; AOffset:Cardinal):WideString;
Var
  W : PWChar;
begin
W := PWChar(NativeInt(AStart) + AOffset);
Result := Copy(WideString(W), 1, Strlen(W));
end;



Procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
DestroySnapshot;
FDeviceTable.Free;
FDriverTable.Free;
end;

Procedure TForm1.FormCreate(Sender: TObject);
Var
  I : Integer;
begin
FDriverTable := TAddressHashTable.Create(251, FreeDriverRecord);
FDeviceTable := TAddressHashTable.Create(255, FreeDeviceRecord);
For I := 0 To IRP_MJ_MAXIMUM_FUNCTION Do
  begin
  With MajorFunctionListView.Items.Add Do
    begin
    Caption := IrpMajorToStr(I);
    SubItems.Add('');
    end;
  end;

Createsnapshot1Click(CreateSnapshot1);
end;

Procedure TForm1.FreeDeviceRecord(ARecord:Pointer);
Var
  R : PDeviceSnapshot;
begin
R := ARecord;
SetLength(R.LowerDevices, 0);
SetLength(R.UpperDevices, 0);
Dispose(R);
end;

Procedure TForm1.FreeDriverRecord(ARecord:Pointer);
Var
  R : PDriverSnapshot;
begin
R := ARecord;
SetLength(R.Devices, 0);
Dispose(R);
end;

Procedure TForm1.DestroySnapshot;
begin
FDriverTable.Clear;
FDeviceTable.Clear;
If Assigned(FSnapshot) Then
  begin
  SetLength(FSnapshot.Drivers, 0);
  Dispose(FSnapshot);
  FSnapshot := Nil;
  end;
end;

Procedure TForm1.Exit1Click(Sender: TObject);
begin
Close;
end;

Procedure TForm1.CreateSnapshot;
Var
  Snapshot : Pointer;
  DriverRecord : PDriverSnapshot;
  DeviceRecord : PDeviceSnapshot;

  Ret : Boolean;
  DriverList : PSNAPSHOT_DRIVERLIST;
  DriverInfo : PSNAPSHOT_DRIVERINFO;
  DeviceInfo : PSNAPSHOT_DEVICEINFO;
  DriverInfoHandle : PPointer;
  DeviceInfoHandle : PPointer;
  I, J, K : Integer;
  pDevice : PPointer;
begin
Ret := DriverCreateSnapshot(Snapshot);
If Ret Then
  begin
  Ret := DriverReadSnapshot(stDriverList, Snapshot, Pointer(DriverList));
  If Ret Then
    begin
    New(FSnapshot);
    FSnapshot.NumberOfDrivers := DriverList.NumberOfDrivers;
    SetLength(FSnapshot.Drivers, FSnapshot.NumberOfDrivers);
    DriverInfoHandle := PPointer(NativeInt(DriverList) + DriverList.DriversOffset);
    For I := 0 To DriverList.NumberOfDrivers - 1 Do
      begin
      Ret := DriverReadSnapshot(stDriverInfo, DriverInfoHandle^, Pointer(DriverInfo));
      If Ret Then
        begin
        FSnapshot.Drivers[I] := DriverInfo.ObjectAddress;

        New(DriverRecord);
        DriverRecord.Address := DriverInfo.ObjectAddress;
        DriverRecord.Name := CopyString(DriverInfo, DriverInfo.NameOffset);
        DriverRecord.MajorFunction := DriverInfo.MajorFunction;
        DriverRecord.NumberOfDevices := DriverInfo.NumberOfDevices;
        SetLength(DriverRecord.Devices, DriverRecord.NumberOfDevices);
        DeviceInfoHandle := PPointer(NativeInt(DriverInfo) + DriverInfo.DevicesOffset);
        For J := 0 To DriverRecord.NumberOfDevices - 1 Do
          begin
          Ret := DriverReadSnapshot(stDeviceInfo, DeviceInfoHandle^, Pointer(DeviceInfo));
          If Ret Then
            begin
            DriverRecord.Devices[J] := DeviceInfo.ObjectAddress;

            New(DeviceRecord);
            DeviceRecord.Address := DeviceInfo.ObjectAddress;
            DeviceRecord.Name := CopyString(DeviceInfo, DeviceInfo.NameOffset);
            DeviceRecord.DriverName := DriverRecord.Name;
            DeviceRecord.DriverAddress := DriverRecord.Address;
            DeviceRecord.DisplayName := CopyString(DeviceInfo, DeviceInfo.DisplayNameOffset);
            DeviceRecord.Description := CopyString(DeviceInfo, DeviceInfo.DescriptionOffset);
            DeviceRecord.Vendor := CopyString(DeviceInfo, DeviceInfo.VendorNameOffset);
            DeviceRecord.Location := CopyString(DeviceInfo, DeviceInfo.LocationOffset);
            DeviceRecord.ClassName := CopyString(DeviceInfo, DeviceInfo.ClassNameOffset);
            DeviceRecord.Enumerator := CopyString(DeviceInfo, DeviceInfo.EnumeratorOffset);
            DeviceRecord.ClassGuid := CopyString(deviceInfo, DeviceInfo.ClassGuidOffset);
            DeviceRecord.Flags := DeviceInfo.Flags;
            DeviceRecord.Characteristics := DeviceInfo.Characteristics;
            DeviceRecord.DeviceType := DeviceInfo.DeviceType;
            DeviceRecord.DiskDeviceAddress := DeviceInfo.DiskDevice;
            DeviceRecord.NumberOfLowerDevices := DeviceInfo.NumberOfLowerDevices;
            DeviceRecord.NumberOfUpperDevices := DeviceInfo.NumberOfUpperDevices;
            SetLength(DeviceRecord.LowerDevices, DeviceRecord.NumberOfLowerDevices);
            SetLength(DeviceRecord.UpperDevices, DeviceRecord.NumberOfUpperDevices);
            pDevice := PPointer(NativeInt(DeviceInfo) + DeviceInfo.LowerDevicesOffset);
            For K := 0 To DeviceRecord.NumberOfLowerDevices - 1 Do
              begin
              DeviceRecord.LowerDevices[K] := pDevice^;
              Inc(pDevice);
              end;

            pDevice := PPointer(NativeInt(DeviceInfo) + DeviceInfo.UpperDevicesOffset);
            For K := 0 To DeviceRecord.NumberOfUpperDevices - 1 Do
              begin
              DeviceRecord.UpperDevices[K] := pDevice^;
              Inc(pDevice);
              end;

            FDeviceTable.Insert(DeviceRecord.Address, DeviceRecord);
            HeapFree(GetProcessHeap, 0, DeviceInfo);
            end;

          Inc(DeviceInfoHandle);
          end;

        FDriverTable.Insert(DriverRecord.Address, DriverRecord);
        HeapFree(GetProcessHeap, 0, DriverInfo);
        end;

      Inc(DriverInfoHandle);
      end;

    HeapFree(GetProcessHeap, 0, DriverList);
    end;

  DriverFreeSnapshot(Snapshot);
  end;
end;

Procedure TForm1.Createsnapshot1Click(Sender: TObject);
begin
DriverDeviceTreeView.Items.Clear;
DestroySnapshot;
CreateSnapshot;
DisplaySnapshot;
end;

Function TForm1.CreateDeviceNode(ADeviceRecord:PDeviceSnapshot; AParent:TTreeNode; ANodeType:TDeviceNodeType):TTreeNode;
Var
  DeviceName : WideString;
begin
If ADeviceRecord.Name <> '' Then
  DeviceName := Format('%s (0x%p) - %s', [ADeviceRecord.Name, ADeviceRecord.Address, ADeviceRecord.DriverName])
Else DeviceName := Format('<unnamed> (0x%p) - %s', [ADeviceRecord.Address, ADeviceRecord.DriverName]);

Case ANodeType Of
  dntUpper : DeviceName := 'UPP: ' + DeviceName;
  dntLower : DeviceName := 'LOW: ' + DeviceName;
  end;

Result := DriverDeviceTreeView.Items.AddChild(AParent, DeviceName);
Result.Data := ADeviceRecord;
end;

Function TForm1.CreateDeviceNodes(ADeviceRecord:PDeviceSnapshot; AParent:TTreeNode):TTreeNode;
Var
  I : Integer;
  DevNode : PDeviceSnapshot;
begin
For I := ADeviceRecord.NumberOfLowerDevices - 1 DownTo 0 Do
  begin
  DevNode := FDeviceTable.Get(ADeviceRecord.LowerDevices[I]);
  If Assigned(DevNode) Then
    AParent := CreateDeviceNode(DevNode, AParent, dntLower)
  Else AParent := DriverDeviceTreeView.Items.AddChild(AParent, Format('LOW: <unknown> (0x%p)', [ADeviceRecord.LowerDevices[I]]));
  end;

AParent := CreateDeviceNode(ADeviceRecord, AParent, dntCurrent);

For I := ADeviceRecord.NumberOfUpperDevices - 1 Downto 0 Do
  begin
  DevNode := FDeviceTable.Get(ADeviceRecord.UpperDevices[I]);
  If Assigned(DevNode) Then
    AParent := CreateDeviceNode(DevNode, AParent, dntUpper)
  Else AParent := DriverDeviceTreeView.Items.AddChild(AParent, Format('UPP: <unknown> (0x%p)', [ADeviceRecord.UpperDevices[I]]));
  end;
end;

Function TForm1.CreateDriverNode(ADriverRecord:PDriverSnapshot):TTreeNode;
Var
  I : Integer;
  DevRecord : PDeviceSnapshot;
begin
Result := DriverDeviceTreeView.Items.AddChild(Nil, ADriverRecord.Name);
Result.Data := ADriverRecord;
For I := 0 To ADriverRecord.NumberOfDevices - 1 Do
  begin
  DevRecord := FDeviceTable.Get(ADriverRecord.Devices[I]);
  If Assigned(DevRecord) Then
    CreateDeviceNodes(DevRecord, Result)
  Else begin
    With DriverDeviceTreeView.Items.AddChild(Result, Format('<unknown> (0x%p)', [ADriverRecord.Devices[I]])) Do
      Data := Nil;
    end;
  end;
end;

Procedure TForm1.DisplaySnapshot;
Var
  I : Integer;
  DriverRecord : PDriverSnapshot;
begin
DriverDeviceTreeView.SortType := stNone;
For I := 0 To FSnapshot.NumberOfDrivers - 1 Do
  begin
  DriverRecord := FDriverTable.Get(FSnapshot.Drivers[I]);
  If Assigned(DriverRecord) Then
    CreateDriverNode(DriverRecord)
  Else begin
    With DriverDeviceTreeView.Items.AddChild(Nil, Format('<unknown> (0x%p)', [FSnapshot.Drivers[I]])) Do
      Data := Nil;
    end;
  end;

DriverDeviceTreeView.SortType := stText;
end;


Procedure TForm1.DriverDeviceTreeViewChange(Sender: TObject; Node: TTreeNode);
begin
PageControl1.Visible := Node.Selected;
If Node.Selected Then
  begin
  DriverTabSheet.TabVisible := Not Assigned(Node.Parent);
  DeviceTabSheet.TabVisible := Assigned(Node.Parent);
  If Assigned(Node.Parent) Then
    begin
    If Assigned(Node.Data) Then
      DisplayDeviceInfo(Node.Data);
    end
  Else begin
    If Assigned(Node.Data) Then
      DisplayDriverInfo(Node.Data);
    end;
  end;
end;

Procedure TForm1.DisplayDriverInfo(ADriverRecord:PDriverSnapshot);
Var
  I : Integer;
  DevNode : PDeviceSnapshot;
  DeviceName : WideString;
begin
DriverAddressLEdit.Text := Format('0x%p', [ADriverRecord.Address]);
DriverNameLEdit.Text := ADriverRecord.Name;
For I := 0 To IRP_MJ_MAXIMUM_FUNCTION Do
  MajorFunctionListview.Items[I].SubItems[0] := Format('0x%p', [ADriverRecord.MajorFunction[I]]);

DriverDevicesListView.Items.BeginUpdate;
DriverDevicesListView.Clear;
For I := 0 To ADriverRecord.NumberOfDevices - 1 Do
  begin
  DevNode := FDeviceTable.Get(ADriverRecord.Devices[I]);
  If Assigned(DevNode) Then
    DeviceName := DevNode.Name
  Else DeviceName := '<unknown>';

  With DriverDevicesListView.Items.Add Do
    begin
    Caption := DeviceName;
    SubItems.Add(Format('0x%p', [ADriverRecord.Devices[I]]));
    Data := DevNode;
    end;
  end;

DriverDevicesListView.Items.EndUpdate;
end;

Procedure TForm1.DisplayDeviceInfo(ADeviceRecord:PDeviceSnapshot);
Var
  DiskDevNode : PDeviceSnapshot;
  DiskDevName : WideString;
begin
Edit1.Text := Format('0x%p', [ADeviceRecord.Address]);
Edit2.Text := ADeviceRecord.Name;
Edit3.Text := Format('0x%p', [ADeviceRecord.DriverAddress]);
Edit4.Text := ADeviceRecord.DriverName;
Edit5.Text := Format('%d', [ADeviceRecord.DeviceType]);
Edit6.Text := Format('0x%x', [ADeviceRecord.Flags]);
Edit7.Text := Format('0x%x', [ADeviceRecord.Characteristics]);
If Assigned(ADeviceRecord.DiskDeviceAddress) Then
  begin
  DiskDevNode := FDeviceTable.Get(ADeviceRecord.DiskDeviceAddress);
  If Assigned(DiskDevNode) Then
    DiskDevName := Format('%S (0x%p)', [DiskDevNode.Name, ADeviceRecord.DiskDeviceAddress])
  Else DiskDevName := Format('<unknown> (0x%p)', [ADeviceRecord.DiskDeviceAddress]);
  end
Else DiskDevName:= Format('<none> (0x%p)', [Nil]);

Edit8.Text := DiskDevName;

CheckBox1.Checked := (ADeviceRecord.Flags And DO_BUFFERED_IO) <> 0;
CheckBox2.Checked := (ADeviceRecord.Flags And DO_DIRECT_IO) <> 0;
CheckBox3.Checked := (ADeviceRecord.Flags And DO_BUS_ENUMERATED_DEVICE) <> 0;
CheckBox4.Checked := (ADeviceRecord.Flags And DO_DEVICE_INITIALIZING) <> 0;
CheckBox5.Checked := (ADeviceRecord.Flags And DO_EXCLUSIVE) <> 0;
CheckBox6.Checked := (ADeviceRecord.Flags And DO_MAP_IO_BUFFER) <> 0;
CheckBox7.Checked := (ADeviceRecord.Flags And DO_POWER_INRUSH) <> 0;
CheckBox8.Checked := (ADeviceRecord.Flags And DO_POWER_PAGABLE) <> 0;
CheckBox9.Checked := (ADeviceRecord.Flags And DO_SHUTDOWN_REGISTERED) <> 0;
CheckBox10.Checked := (ADeviceRecord.Flags And DO_VERIFY_VOLUME) <> 0;
CheckBox23.Checked := (ADeviceRecord.Flags And DO_DEVICE_HAS_NAME) <> 0;
CheckBox24.Checked := (ADeviceRecord.Flags And DO_SYSTEM_BOOT_PARTITION) <> 0;
CheckBox25.Checked := (ADeviceRecord.Flags And DO_LONG_TERM_REQUESTS) <> 0;
CheckBox26.Checked := (ADeviceRecord.Flags And DO_NEVER_LAST_DEVICE) <> 0;
CheckBox27.Checked := (ADeviceRecord.Flags And DO_LOW_PRIORITY_FILESYSTEM) <> 0;
CheckBox28.Checked := (ADeviceRecord.Flags And DO_SUPPORTS_TRANSACTIONS) <> 0;
CheckBox29.Checked := (ADeviceRecord.Flags And DO_FORCE_NEITHER_IO) <> 0;
CheckBox30.Checked := (ADeviceRecord.Flags And DO_VOLUME_DEVICE_OBJECT) <> 0;
CheckBox31.Checked := (ADeviceRecord.Flags And DO_SYSTEM_SYSTEM_PARTITION) <> 0;
CheckBox32.Checked := (ADeviceRecord.Flags And DO_SYSTEM_CRITICAL_PARTITION) <> 0;

CheckBox11.Checked := (ADeviceRecord.Characteristics And FILE_AUTOGENERATED_DEVICE_NAME) <> 0;
CheckBox12.Checked := (ADeviceRecord.Characteristics And FILE_CHARACTERISTIC_PNP_DEVICE) <> 0;
CheckBox13.Checked := (ADeviceRecord.Characteristics And FILE_CHARACTERISTIC_TS_DEVICE) <> 0;
CheckBox14.Checked := (ADeviceRecord.Characteristics And FILE_CHARACTERISTIC_WEBDAV_DEVICE) <> 0;
CheckBox15.Checked := (ADeviceRecord.Characteristics And FILE_DEVICE_IS_MOUNTED) <> 0;
CheckBox16.Checked := (ADeviceRecord.Characteristics And FILE_DEVICE_SECURE_OPEN) <> 0;
CheckBox17.Checked := (ADeviceRecord.Characteristics And FILE_FLOPPY_DISKETTE) <> 0;
CheckBox18.Checked := (ADeviceRecord.Characteristics And FILE_READ_ONLY_DEVICE) <> 0;
CheckBox19.Checked := (ADeviceRecord.Characteristics And FILE_REMOTE_DEVICE) <> 0;
CheckBox20.Checked := (ADeviceRecord.Characteristics And FILE_REMOVABLE_MEDIA) <> 0;
CheckBox21.Checked := (ADeviceRecord.Characteristics And FILE_VIRTUAL_VOLUME) <> 0;
CheckBox22.Checked := (ADeviceRecord.Characteristics And FILE_WRITE_ONCE_MEDIA) <> 0;

With DevicePnpListView Do
  begin
  Items[0].SubItems[0] := ADeviceRecord.DisplayName;
  Items[1].SubItems[0] := ADeviceRecord.Description;
  Items[2].SubItems[0] := ADeviceRecord.Vendor;
  Items[3].SubItems[0] := ADeviceRecord.ClassName;
  Items[4].SubItems[0] := ADeviceRecord.ClassGuid;
  Items[5].SubItems[0] := ADeviceRecord.Location;
  Items[6].SubItems[0] := ADeviceRecord.Enumerator;
  end;
end;

End.

