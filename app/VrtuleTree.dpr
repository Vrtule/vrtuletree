program VrtuleTree;

uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {Form1},
  scmDrivers in 'scmDrivers.pas',
  VTreeDriver in 'VTreeDriver.pas',
  HashTable in 'HashTable.pas',
  Kernel in 'Kernel.pas';

{$R *.res}

Var
  Ret : Boolean;
begin
Application.Initialize;
DriverUninstall;
DriverInstall;
Ret := DriverLoad;
If Ret Then
  begin
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
  DriverUnload;
  end;

DriverUninstall;
end.
