Unit HashTable;

Interface

Type
  PHashItem = ^THashItem;
  THashItem = Record
    Next : PHashItem;
    Address : Pointer;
    Item : Pointer;
    end;

  TOnFreeItem = Procedure (AItem:Pointer) Of Object;

  TAddressHashTable = Class
  Private
    FTableSIze : Integer;
    FTable : Array Of PHashItem;
    FOnFreeItem : TOnFreeItem;
    Function Hash(AAddress:Pointer):Cardinal;
  Public
    Procedure Clear;
    Procedure Insert(AAddress:Pointer; AItem:Pointer);
    Function Get(AAddress:Pointer):Pointer;
    Function Delete(AAddress:Pointer):Pointer;
    Constructor Create(ASize:Integer; AOnFreeitem:TOnFreeItem = Nil); Reintroduce;
    Destructor Destroy; Override;

    Property OnFreeItem : TOnFreeItem Read FOnFreeItem;
  end;


Implementation


Function TAddressHashTable.Hash(AAddress:Pointer):Cardinal;
Var
 AddressNum : NativeUInt;
begin
AddressNum := NativeUInt(AAddress);
Result :=
   ((AddressNum * 13) +
   ((AddressNum Shr 8) * 17) +
   ((AddressNum Shr 16) * 23) +
   ((AddressNum Shr 24) * 29)) Mod FTableSize;
end;

Procedure TAddressHashTable.Insert(AAddress:Pointer; AItem:Pointer);
Var
 Index : Integer;
 HashItem : PHashItem;
begin
New(HashItem);
HashItem.Address := AAddress;
HashItem.Item := AItem;
Index := Hash(AAddress);
HashItem.Next := FTable[Index];
FTable[Index] := HashItem;
end;

Function TAddressHashTable.Get(AAddress:Pointer):Pointer;
Var
  Tmp : PHashItem;
begin
Result := Nil;
Tmp := FTable[Hash(AAddress)];
While Assigned(Tmp) Do
  begin
  If Tmp.Address = AAddress Then
    begin
    Result := Tmp.Item;
    Break;
    end;

  Tmp := Tmp.Next;
  end;
end;

Function TAddressHashTable.Delete(AAddress:Pointer):Pointer;
Var
  Tmp : PHashItem;
  Prev : PHashItem;
  Index : Integer;
begin
Result := Nil;
Prev := Nil;
Index := Hash(AAddress);
Tmp := FTable[Index];
While Assigned(Tmp) Do
  begin
  If Tmp.Address = AAddress Then
    begin
    Result := Tmp.Item;
    If Assigned(Prev) Then
      Prev.Next := Tmp.Next
    Else FTable[Index] := Tmp.Next;

    Dispose(Tmp);
    Break;
    end;

  Prev := Tmp;
  Tmp := Tmp.Next;
  end;
end;

Constructor TAddressHashTable.Create(ASize:Integer; AOnFreeItem:TOnFreeItem = Nil);
Var
  I : Integer;
begin
Inherited Create;
FOnFreeItem := AOnFreeItem;
FTableSize := ASize;
SetLength(FTable, FTableSize);
For I := 0 To FTableSize - 1 Do
  FTable[I] := Nil;
end;

Destructor TAddressHashTable.Destroy;
begin
Clear;
SetLength(FTable, 0);
Inherited Destroy;
end;

Procedure TAddressHashTable.Clear;
Var
 I : Integer;
 Tmp : PHashItem;
 Old : PHashItem;
begin
For I := 0 To FTableSize - 1 Do
  begin
  Tmp := FTable[I];
  While Assigned(Tmp) Do
    begin
    Old := Tmp;
    Tmp := Tmp.Next;
    If Assigned(FOnFreeItem) Then
      FOnFreeItem(Old.Item);

    Dispose(Old);
    end;

  FTable[I] := Nil;
  end;
end;


End.
