object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'VrtuleTree v1.0'
  ClientHeight = 504
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DriverDeviceTreeView: TTreeView
    Left = 0
    Top = 0
    Width = 131
    Height = 504
    Align = alLeft
    Indent = 19
    ReadOnly = True
    SortType = stText
    TabOrder = 0
    OnChange = DriverDeviceTreeViewChange
  end
  object PageControl1: TPageControl
    Left = 131
    Top = 0
    Width = 471
    Height = 504
    ActivePage = DriverTabSheet
    Align = alClient
    TabOrder = 1
    object DeviceTabSheet: TTabSheet
      Caption = 'Device'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 463
        Height = 137
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 2
          Top = 8
          Width = 39
          Height = 13
          Caption = 'Address'
        end
        object Label2: TLabel
          Left = 3
          Top = 35
          Width = 27
          Height = 13
          Caption = 'Name'
        end
        object Label3: TLabel
          Left = 247
          Top = 8
          Width = 71
          Height = 13
          Caption = 'Driver Address'
        end
        object Label4: TLabel
          Left = 247
          Top = 35
          Width = 59
          Height = 13
          Caption = 'Driver Name'
        end
        object Label5: TLabel
          Left = 2
          Top = 62
          Width = 59
          Height = 13
          Caption = 'Device Type'
        end
        object Label6: TLabel
          Left = 3
          Top = 89
          Width = 25
          Height = 13
          Caption = 'Flags'
        end
        object Label7: TLabel
          Left = 247
          Top = 89
          Width = 71
          Height = 13
          Caption = 'Characteristics'
        end
        object Label8: TLabel
          Left = 2
          Top = 116
          Width = 56
          Height = 13
          Caption = 'Disk Volume'
        end
        object Edit1: TEdit
          Left = 104
          Top = 0
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object Edit2: TEdit
          Left = 104
          Top = 27
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object Edit3: TEdit
          Left = 336
          Top = 0
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object Edit4: TEdit
          Left = 336
          Top = 27
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
        object Edit5: TEdit
          Left = 104
          Top = 54
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 4
        end
        object Edit6: TEdit
          Left = 104
          Top = 81
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 5
        end
        object Edit7: TEdit
          Left = 336
          Top = 81
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 6
        end
        object Edit8: TEdit
          Left = 104
          Top = 108
          Width = 353
          Height = 21
          ReadOnly = True
          TabOrder = 7
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 137
        Width = 463
        Height = 104
        Align = alTop
        Caption = 'Flags'
        TabOrder = 1
        object CheckBox1: TCheckBox
          Left = 3
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Buffered I/O'
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 3
          Top = 32
          Width = 97
          Height = 17
          Caption = 'Direct I/O'
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 3
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Enumerated'
          TabOrder = 2
        end
        object CheckBox4: TCheckBox
          Left = 115
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Initializing'
          TabOrder = 3
        end
        object CheckBox5: TCheckBox
          Left = 115
          Top = 32
          Width = 97
          Height = 17
          Caption = 'Exclusive'
          TabOrder = 4
        end
        object CheckBox6: TCheckBox
          Left = 115
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Map I/O Buffer'
          TabOrder = 5
        end
        object CheckBox7: TCheckBox
          Left = 235
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Power Inrush'
          TabOrder = 6
        end
        object CheckBox8: TCheckBox
          Left = 235
          Top = 32
          Width = 97
          Height = 17
          Caption = 'Power Pageable'
          TabOrder = 7
        end
        object CheckBox9: TCheckBox
          Left = 235
          Top = 48
          Width = 126
          Height = 17
          Caption = 'Shutdown Registered'
          TabOrder = 8
        end
        object CheckBox10: TCheckBox
          Left = 3
          Top = 64
          Width = 97
          Height = 17
          Caption = 'Verify Volume'
          TabOrder = 9
        end
        object CheckBox23: TCheckBox
          Left = 115
          Top = 64
          Width = 73
          Height = 17
          Caption = 'Named'
          TabOrder = 10
        end
        object CheckBox24: TCheckBox
          Left = 235
          Top = 64
          Width = 120
          Height = 17
          Caption = 'Boot Partition'
          TabOrder = 11
        end
        object CheckBox25: TCheckBox
          Left = 363
          Top = 16
          Width = 120
          Height = 17
          Caption = 'Long Term'
          TabOrder = 12
        end
        object CheckBox26: TCheckBox
          Left = 363
          Top = 32
          Width = 120
          Height = 17
          Caption = 'Never Last'
          TabOrder = 13
        end
        object CheckBox27: TCheckBox
          Left = 363
          Top = 48
          Width = 120
          Height = 17
          Caption = 'Low Priority FS'
          TabOrder = 14
        end
        object CheckBox28: TCheckBox
          Left = 363
          Top = 64
          Width = 120
          Height = 17
          Caption = 'Transactions'
          TabOrder = 15
        end
        object CheckBox29: TCheckBox
          Left = 3
          Top = 80
          Width = 106
          Height = 17
          Caption = 'Force Neither I/O'
          TabOrder = 16
        end
        object CheckBox30: TCheckBox
          Left = 115
          Top = 80
          Width = 120
          Height = 17
          Caption = 'Volume'
          TabOrder = 17
        end
        object CheckBox31: TCheckBox
          Left = 235
          Top = 80
          Width = 120
          Height = 17
          Caption = 'System Partition'
          TabOrder = 18
        end
        object CheckBox32: TCheckBox
          Left = 363
          Top = 81
          Width = 120
          Height = 17
          Caption = 'Critical Partition'
          TabOrder = 19
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 241
        Width = 463
        Height = 95
        Align = alTop
        Caption = 'Characteristics'
        TabOrder = 2
        object CheckBox11: TCheckBox
          Left = 3
          Top = 24
          Width = 126
          Height = 17
          Caption = 'Autogenerated Name'
          TabOrder = 0
        end
        object CheckBox12: TCheckBox
          Left = 3
          Top = 40
          Width = 126
          Height = 17
          Caption = 'Characteristic PnP'
          TabOrder = 1
        end
        object CheckBox13: TCheckBox
          Left = 3
          Top = 56
          Width = 142
          Height = 17
          Caption = 'Characteristic TS'
          TabOrder = 2
        end
        object CheckBox14: TCheckBox
          Left = 3
          Top = 72
          Width = 142
          Height = 17
          Caption = 'Characteristic WebDAV'
          TabOrder = 3
        end
        object CheckBox15: TCheckBox
          Left = 147
          Top = 24
          Width = 94
          Height = 17
          Caption = 'Mounted'
          TabOrder = 4
        end
        object CheckBox16: TCheckBox
          Left = 147
          Top = 40
          Width = 94
          Height = 17
          Caption = 'Secure Open'
          TabOrder = 5
        end
        object CheckBox17: TCheckBox
          Left = 147
          Top = 56
          Width = 94
          Height = 17
          Caption = 'Diskette'
          TabOrder = 6
        end
        object CheckBox18: TCheckBox
          Left = 147
          Top = 72
          Width = 94
          Height = 17
          Caption = 'Read Only'
          TabOrder = 7
        end
        object CheckBox19: TCheckBox
          Left = 247
          Top = 24
          Width = 126
          Height = 17
          Caption = 'Remote'
          TabOrder = 8
        end
        object CheckBox20: TCheckBox
          Left = 247
          Top = 40
          Width = 126
          Height = 17
          Caption = 'Removable'
          TabOrder = 9
        end
        object CheckBox21: TCheckBox
          Left = 247
          Top = 56
          Width = 126
          Height = 17
          Caption = 'Virtual Volume'
          TabOrder = 10
        end
        object CheckBox22: TCheckBox
          Left = 247
          Top = 72
          Width = 126
          Height = 17
          Caption = 'Write Once'
          TabOrder = 11
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 336
        Width = 463
        Height = 144
        Align = alTop
        Caption = 'Pnp'
        TabOrder = 3
        object DevicePnpListView: TListView
          Left = 2
          Top = 15
          Width = 459
          Height = 127
          Align = alClient
          Columns = <
            item
              Caption = 'Name'
              Width = 100
            end
            item
              AutoSize = True
              Caption = 'Value'
            end>
          Items.ItemData = {
            05630100000700000000000000FFFFFFFFFFFFFFFF01000000FFFFFFFF000000
            000C44006900730070006C006100790020004E0061006D00650000604E7C1400
            000000FFFFFFFFFFFFFFFF01000000FFFFFFFF000000000B4400650073006300
            720069007000740069006F006E000098247C1400000000FFFFFFFFFFFFFFFF01
            000000FFFFFFFF0000000006560065006E0064006F00720000E82C7C14000000
            00FFFFFFFFFFFFFFFF01000000FFFFFFFF000000000543006C00610073007300
            0048387C1400000000FFFFFFFFFFFFFFFF01000000FFFFFFFF000000000A4300
            6C006100730073002000470055004900440000C02F7C1400000000FFFFFFFFFF
            FFFFFF01000000FFFFFFFF00000000084C006F0063006100740069006F006E00
            0058117C1400000000FFFFFFFFFFFFFFFF01000000FFFFFFFF000000000A4500
            6E0075006D0065007200610074006F0072000060DE7B14FFFFFFFFFFFFFFFFFF
            FFFFFFFFFF}
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
    end
    object DriverTabSheet: TTabSheet
      Caption = 'Driver'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 49
        Width = 463
        Height = 128
        Align = alTop
        Caption = 'Devices'
        TabOrder = 0
        object DriverDevicesListView: TListView
          Left = 2
          Top = 15
          Width = 459
          Height = 111
          Align = alClient
          Columns = <
            item
              AutoSize = True
              Caption = 'Name'
            end
            item
              Caption = 'Address'
              Width = 150
            end>
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 177
        Width = 463
        Height = 249
        Align = alTop
        Caption = 'Major Function'
        TabOrder = 1
        object MajorFunctionListview: TListView
          Left = 2
          Top = 15
          Width = 459
          Height = 232
          Align = alClient
          Columns = <
            item
              AutoSize = True
              Caption = 'Function'
            end
            item
              Caption = 'Address'
              Width = 150
            end>
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 463
        Height = 49
        Align = alTop
        TabOrder = 2
        object DriverAddressLEdit: TLabeledEdit
          Left = 2
          Top = 22
          Width = 167
          Height = 21
          EditLabel.Width = 39
          EditLabel.Height = 13
          EditLabel.Caption = 'Address'
          TabOrder = 0
        end
        object DriverNameLEdit: TLabeledEdit
          Left = 184
          Top = 22
          Width = 169
          Height = 21
          EditLabel.Width = 27
          EditLabel.Height = 13
          EditLabel.Caption = 'Name'
          TabOrder = 1
        end
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 536
    Top = 120
    object File1: TMenuItem
      Caption = 'File'
      object Createsnapshot1: TMenuItem
        Caption = 'Create snapshot'
        OnClick = Createsnapshot1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
  end
end
