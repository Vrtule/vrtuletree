
#include <ntifs.h>
#include "preprocessor.h"
#include "driver.h"
#include "utils.h"
#include "snapshot.h"


/************************************************************************/
/*                            GLOBAL VARIABLES                                          */
/************************************************************************/

/** All existing snapshots are stored inside a linked list. */
static LIST_ENTRY SnapshotList;
/** Synchronizes access to the list of snapshots. */
static FAST_MUTEX SnapshotListMutex;


/************************************************************************/
/* DRIVERLIST SNAPSHOT ROUTINES                                                                     */
/************************************************************************/

/** Creates Driver List structure that describes a list of drivers in the
 *  snapshot. On input, the routine accepts DRIVER_OBJECT structures of the
 *  drivers it should include into the snapshot.
 *
 *  @param DriverArrays An array of arrays of PDRIVER_OBJECT pointers. For
 *  example, every array of the pointers might contain Driver Objects from
 *  one directory.
 *  @param ArrayLengths Number of elements in the arrays of Driver Object
 *  pointers.
 *  @param ItemCount Number of arrays of Driver Object pointers.
 *  @param DriverList Pointer to variable that, in case the routine succeeds,
 *  receives address of newly created Driver List structure. If the routine
 *  fails, the parameter is filled with NULL.
 *
 *  @return Returns NTSTATUS value indicating either success or failure of
 *  the operation.
 */
static NTSTATUS _DriverListNodeCreate(PDRIVER_OBJECT **DriverArrays, PSIZE_T ArrayLengths, SIZE_T ItemCount, PSNAPSHOT_DRIVERLIST *DriverList)
{
   LONG i = 0;
   ULONG_PTR DriverCount = 0;
   PSNAPSHOT_DRIVERLIST TmpDriverList = NULL;
   SIZE_T DriverListSize = 0;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DriverArrays=0x%p; ArrayLengths=0x%p; ItemCount=%u; DriverList=0x%p", DriverArrays, ArrayLengths, ItemCount, DriverList);

   *DriverList = NULL;
   // Compute the total number of Driver Object pointers stored inside
   // the arrays
   for (i = 0; i < ItemCount; ++i) 
      DriverCount += ArrayLengths[i];
   
   // Compute the total size of the new Driver List structure and allocate
   // storage for it. 
   DriverListSize = sizeof(SNAPSHOT_DRIVERLIST) + DriverCount * sizeof(PVOID);
   TmpDriverList = (PSNAPSHOT_DRIVERLIST)ExAllocatePool(PagedPool, DriverListSize);
   if (TmpDriverList != NULL) {
      PDRIVER_OBJECT *Data = NULL;
      
      // Initialize the structure
      TmpDriverList->Size = DriverListSize;
      TmpDriverList->NumberOfDrivers = DriverCount;
      TmpDriverList->DriversOffset = sizeof(SNAPSHOT_DRIVERLIST);
      Data = (PDRIVER_OBJECT *)((PUCHAR)TmpDriverList + TmpDriverList->DriversOffset);
      // Copy addresses of Driver Objects from the arrays
      for (i = 0; i < ItemCount; ++i) {
         RtlCopyMemory(Data, DriverArrays[i], ArrayLengths[i] * sizeof(PDRIVER_OBJECT));
         Data += ArrayLengths[i];
      }

      // Report success and fill the output argument
      Status = STATUS_SUCCESS;
      *DriverList = TmpDriverList;
   } else Status = STATUS_INSUFFICIENT_RESOURCES;

   DEBUG_EXIT_FUNCTION("0x%x, *DriverList=0x%p", Status, *DriverList);
   return Status;
}

/************************************************************************/
/* DEVICEINFO SNAPSHOT ROUTINES                                                                     */
/************************************************************************/

/** Retrieves PnP information about a device object. The data are
 *  retrieved from the registry.
 *
 *  @param DeviceObject Address of Device Object structure to examine.
 *  @param DisplayName Receives a wide character string containing human-readable
 *  name of the device.
 *  @param Receives a wide character string containing human-readable description
 *  of the device.
 *  @param VendorName Receives a wide character string containing name of the
 *  vendor. 
 *  @param Enumerator Receives a wide character string containing the name
 *  of the component that enumerated the device.
 *  @param ClassName Receives a wide character string containing the name
 *  of the class of the device.
 *  @param ClassGuid Receives a wide character string containing the GUID
 *  of the device class.
 *  @param Location Receives a wide character string containing human-readable
 *  information about the location of the device on its bus.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the operation.
 *
 *  @remark If the registry does not contain certain data in the registry,
 *  the routine fills the corresponding parameter with NULL.
 */
static NTSTATUS _GetDevicePnPInformation(
   PDEVICE_OBJECT DeviceObject,
   PWCHAR *DisplayName,
   PWCHAR *Description,
   PWCHAR *VendorName,
   PWCHAR *Enumerator,
   PWCHAR *ClassName,
   PWCHAR *ClassGuid,
   PWCHAR *Location)
{
   ULONG VendorNameLength = 0;
   ULONG DisplayNameLength = 0;
   ULONG DescriptionLength = 0;
   ULONG ClassLength = 0;
   ULONG EnumeratorLength = 0;
   ULONG LocationLength = 0;
   ULONG ClassGuidLength = 0;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p", DeviceObject);

   Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyClassName, ClassName, &ClassLength);
   if (NT_SUCCESS(Status)) {
      Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyDeviceDescription, Description, &DescriptionLength);
      if (NT_SUCCESS(Status)) {
         Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyFriendlyName, DisplayName, &DisplayNameLength);
         if (NT_SUCCESS(Status)) {
            Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyManufacturer, VendorName, &VendorNameLength);
            if (NT_SUCCESS(Status)) {
               Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyLocationInformation, Location, &LocationLength);
               if (NT_SUCCESS(Status)) {
                  Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyEnumeratorName, Enumerator, &EnumeratorLength);
                  if (NT_SUCCESS(Status)) {
                     Status = _GetWCharDeviceProperty(DeviceObject, DevicePropertyClassGuid, ClassGuid, &ClassGuidLength);
                  }
               }
            }
         }
      }
   }

   if (!NT_SUCCESS(Status)) {
      if (*Enumerator != NULL) {
         ExFreePool(*Enumerator);
         *Enumerator = NULL;
      }

      if (*Location != NULL) {
         ExFreePool(*Location);
         *Location = NULL;
      }

      if (*DisplayName != NULL) {
         ExFreePool(*DisplayName);
         *DisplayName = NULL;
      }

      if (*VendorName != NULL) {
         ExFreePool(*VendorName);
         *VendorName = NULL;
      }

      if (*Description != NULL) {
         ExFreePool(*Description);
         *Description = NULL;
      }

      if (*ClassName != NULL) {
         ExFreePool(*ClassName);
         *ClassName = NULL;
      }

      if (*ClassGuid != NULL) {
         ExFreePool(*ClassGuid);
         *ClassGuid = NULL;
      }

      RtlZeroMemory(ClassGuid, sizeof(GUID));
   }

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}

/** Copies wide character string to the one of snapshot records and updates
 *  position information.
 *
 *  @param String String to copy.
 *  @param Record Address of the beginning of the record to which the string
 *  should be copied.
 *  @param DataStart Pointer to variable that points to place inside the record
 *  where the string should be stored. The routine updates this variable to point
 *  just after the terminating null character of the string.
 *  @param OffsetField Address of field of the record that contains information
 *  about the beginning of the string inside the record. The information have form
 *  of offset to the string relative to beginning of the record. The variable is
 *  updated to contain correct data.
 */
static VOID _CopyString(PWCHAR String, PVOID Record, PUCHAR *DataStart, PULONG_PTR OffsetField)
{
   SIZE_T StrLength = 0;
   DEBUG_ENTER_FUNCTION("String=\"%S\"; Record=0x%p; DataStart=0x%p; OffsetField=0x%p", String, Record, DataStart, OffsetField);

   if (String != NULL)
      StrLength = wcslen(String);

   *OffsetField = (ULONG_PTR)*DataStart - (ULONG_PTR)Record;
   RtlCopyMemory(*DataStart, String, StrLength * sizeof(WCHAR));
   ((PWCHAR)(*DataStart))[StrLength] = L'\0';
   *DataStart += (StrLength + 1) * sizeof(WCHAR);

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/** Creates Device Info record that contains information about a device object
 *  within the snapshot.
 *
 *  @param DeviceObject Address of Device Object information about which should
 *  be collected and stored inside new Device Info record.
 *  @param DeviceInfo Address of variable that, when the routine succeeds,
 *  receives address of the newly allocated and initialized Device Info 
 *  structure. When the routine fails, the variable is filled with NULL.
 */
static NTSTATUS _DeviceInfoSnapshotCreate(PDEVICE_OBJECT DeviceObject, PSNAPSHOT_DEVICE_INFO *DeviceInfo)
{
   UNICODE_STRING DeviceName;
   PSNAPSHOT_DEVICE_INFO TmpDeviceInfo = NULL;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p; DeviceInfo=0x%p", DeviceObject, *DeviceInfo);

   *DeviceInfo = NULL;
   // Retrieve name of the device object
   Status = _GetObjectName(DeviceObject, &DeviceName);
   if (NT_SUCCESS(Status)) {
      PDEVICE_OBJECT *UpperDevices = NULL;
      SIZE_T UpperDeviceCount = 0;
      PDEVICE_OBJECT DiskDevice = NULL;

      // Retrieve Disk Device Object, present in filesystem devices that correspond
      // to volumes.
      Status = IoGetDiskDeviceObject(DeviceObject, &DiskDevice);
      if (!NT_SUCCESS(Status))
         DiskDevice = NULL;

      // Retrieve addresses of Device Objects for lower devices in the
      // device stack.
      Status = _GetLowerUpperDevices(DeviceObject, TRUE, &UpperDevices, &UpperDeviceCount);
      if (NT_SUCCESS(Status)) {
         PDEVICE_OBJECT *LowerDevices = NULL;
         SIZE_T LowerDeviceCount = 0;

         // Retrieve addresses of Device Objects for upper devices in the
         // device stack.
         Status = _GetLowerUpperDevices(DeviceObject, FALSE, &LowerDevices, &LowerDeviceCount);
         if (NT_SUCCESS(Status)) {
            SIZE_T DeviceInfoSize = 0;
            PWCHAR DisplayName = NULL;
            PWCHAR Description = NULL;
            PWCHAR Vendor = NULL;
            PWCHAR Enumerator = NULL;
            PWCHAR Location = NULL;
            PWCHAR Class = NULL;
            PWCHAR ClassGuid = NULL;

            // Start to compute size of the Device Info record
            DeviceInfoSize = sizeof(SNAPSHOT_DEVICE_INFO) + DeviceName.Length + sizeof(WCHAR) + (LowerDeviceCount + UpperDeviceCount) * sizeof(PDEVICE_OBJECT);
            DeviceInfoSize += 7 * sizeof(WCHAR);
            if (DeviceObject->Flags & DO_BUS_ENUMERATED_DEVICE) {
               // The device has been enumerated by PnP. Try to retrieve
               // some information about it.
               Status = _GetDevicePnPInformation(DeviceObject, &DisplayName, &Description, &Vendor, &Enumerator, &Class, &ClassGuid, &Location);
               if (NT_SUCCESS(Status)) {
                  if (DisplayName != NULL)
                     DeviceInfoSize += wcslen(DisplayName) * sizeof(WCHAR);
                  
                  if (Vendor != NULL)
                     DeviceInfoSize += wcslen(Vendor) * sizeof(WCHAR);
                  
                  if (Description != NULL)
                     DeviceInfoSize += wcslen(Description) * sizeof(WCHAR);
                  
                  if (Class != NULL)
                     DeviceInfoSize += wcslen(Class) * sizeof(WCHAR);
                  
                  if (Location != NULL)
                     DeviceInfoSize += wcslen(Location) * sizeof(WCHAR);
                  
                  if (Enumerator != NULL)
                     DeviceInfoSize += wcslen(Enumerator) * sizeof(WCHAR);

                  if (ClassGuid != NULL)
                     DeviceInfoSize += wcslen(ClassGuid) * sizeof(WCHAR);
               }
            }
            
            if (NT_SUCCESS(Status)) {
               // Allocate the Device Info record
               TmpDeviceInfo = (PSNAPSHOT_DEVICE_INFO)ExAllocatePool(PagedPool, DeviceInfoSize);
               if (TmpDeviceInfo != NULL) {
                  PUCHAR Data = NULL;

                  // Copy all information collected about the device into the
                  // record.
                  RtlZeroMemory(TmpDeviceInfo, DeviceInfoSize);
                  TmpDeviceInfo->Size = DeviceInfoSize;
                  TmpDeviceInfo->Characteristics = DeviceObject->Characteristics;
                  TmpDeviceInfo->DeviceType = DeviceObject->DeviceType;
                  TmpDeviceInfo->Flags = DeviceObject->Flags;
                  TmpDeviceInfo->ObjectAddress = DeviceObject;
                  TmpDeviceInfo->NumberOfLowerDevices = LowerDeviceCount;
                  TmpDeviceInfo->NumberOfUpperDevices = UpperDeviceCount;
                  TmpDeviceInfo->NameOffset = sizeof(SNAPSHOT_DEVICE_INFO);
                  Data = (PUCHAR)TmpDeviceInfo + TmpDeviceInfo->NameOffset;
                  RtlCopyMemory(Data, DeviceName.Buffer, DeviceName.Length);
                  ((PWCHAR)Data)[DeviceName.Length / sizeof(WCHAR)] = L'\0';
                  Data += DeviceName.Length + sizeof(WCHAR);
                  TmpDeviceInfo->LowerDevicesOffset = (ULONG_PTR)Data - (ULONG_PTR)TmpDeviceInfo;
                  RtlCopyMemory(Data, LowerDevices, LowerDeviceCount * sizeof(PDEVICE_OBJECT));
                  Data += LowerDeviceCount * sizeof(PDEVICE_OBJECT);
                  TmpDeviceInfo->UpperDevicesOffset = (ULONG_PTR)Data - (ULONG_PTR)TmpDeviceInfo;
                  RtlCopyMemory(Data, UpperDevices, UpperDeviceCount * sizeof(PDEVICE_OBJECT));
                  Data += UpperDeviceCount * sizeof(PDEVICE_OBJECT);
                  _CopyString(Class, TmpDeviceInfo, &Data, &TmpDeviceInfo->ClassNameOffset);
                  _CopyString(DisplayName, TmpDeviceInfo, &Data, &TmpDeviceInfo->DisplayNameOffset);
                  _CopyString(Description, TmpDeviceInfo, &Data, &TmpDeviceInfo->DescriptionOffset);
                  _CopyString(Vendor, TmpDeviceInfo, &Data, &TmpDeviceInfo->VendorNameOffset);
                  _CopyString(Enumerator, TmpDeviceInfo, &Data, &TmpDeviceInfo->EnumeratorOffset);
                  _CopyString(Location, TmpDeviceInfo, &Data, &TmpDeviceInfo->LocationOffset);
                  _CopyString(ClassGuid, TmpDeviceInfo, &Data, &TmpDeviceInfo->ClassGuidOffset);
                  TmpDeviceInfo->DiskDevice = DiskDevice;
                  // Return the Device Info record and signal success of
                  // the operation.
                  *DeviceInfo = TmpDeviceInfo;
                  Status = STATUS_SUCCESS;
               } else Status = STATUS_INSUFFICIENT_RESOURCES;
            
               if (Enumerator != NULL)
                  ExFreePool(Enumerator);

               if (Location != NULL)
                  ExFreePool(Location);

               if (DisplayName != NULL)
                  ExFreePool(DisplayName);

               if (Vendor != NULL)
                  ExFreePool(Vendor);

               if (Description != NULL)
                  ExFreePool(Description);

               if (Class != NULL)
                  ExFreePool(Class);

               if (ClassGuid != NULL)
                  ExFreePool(ClassGuid);
            }

            _ReleaseDeviceArray(LowerDevices, LowerDeviceCount);
         }

         _ReleaseDeviceArray(UpperDevices, UpperDeviceCount);
      }

      ExFreePool(DeviceName.Buffer);
   }

   DEBUG_EXIT_FUNCTION("0x%x, *DeviceInfo=0x%p", Status, *DeviceInfo);
   return Status;
}

/** Frees memory and potentially othe rresources allocated for
 *  a Device Info record.
 *
 *  @param Address of Device Info record to free.
 */
static VOID _DeviceInfoSnapshotFree(PSNAPSHOT_DEVICE_INFO DeviceInfo)
{
   DEBUG_ENTER_FUNCTION("DeviceInfo=0x%p", DeviceInfo);

   ExFreePool(DeviceInfo);

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/************************************************************************/
/* DRIVERINFO SNAPSHOT ROUTINES                                                                     */
/************************************************************************/

/** Creates a Driver Info record that represents given device driver within
 *  the snapshot.
 *
 *  @param DriverObject Address of Driver Object for which the Driver Info
 *  structure should be created.
 *  @param DeviceInfo Address of variable that, if the function succeeds, 
 *  receives address of the new Driver Info structure. If the routine fails,
 *  the variable is filled with NULL.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the opeation.
 *
 *  @remark The routine also determines device objects of given driver, and
 *  creates and fills Device Info structures of them. Addresses of the structures
 *  are stored inside the Driver Info structure.
 */
static NTSTATUS _DriverSnapshotCreate(PDRIVER_OBJECT DriverObject, PSNAPSHOT_DRIVER_INFO *DriverInfo)
{
   UNICODE_STRING DriverName;
   PSNAPSHOT_DRIVER_INFO TmpDriverInfo = NULL;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   PDEVICE_OBJECT *DeviceArray = NULL;
   ULONG DeviceArrayLength = 0;
   DEBUG_ENTER_FUNCTION("DriverObject=0x%p; DriverInfo=0x%p", DriverObject, DriverInfo);
   
   *DriverInfo = NULL;
   // Get name of the driver
   Status = _GetObjectName(DriverObject, &DriverName);
   if (NT_SUCCESS(Status)) {
      // Enum its devices
      Status = _EnumDriverDevices(DriverObject, &DeviceArray, &DeviceArrayLength);
      if (NT_SUCCESS(Status)) {
         LONG i = 0;
         SIZE_T DriverInfoSize = 0;

         // Compute size of the new Driver Info record and allocate it.
         DriverInfoSize = sizeof(SNAPSHOT_DRIVER_INFO) + DriverName.Length + sizeof(WCHAR) + DeviceArrayLength * sizeof(PDEVICE_OBJECT);
         TmpDriverInfo = (PSNAPSHOT_DRIVER_INFO)ExAllocatePool(PagedPool, DriverInfoSize);
         if (TmpDriverInfo != NULL) {
            LONG i = 0;
            PDEVICE_OBJECT *pDeviceObject = NULL;
            PUCHAR Data = (PUCHAR)((PUCHAR)TmpDriverInfo + sizeof(SNAPSHOT_DRIVER_INFO));
            
            // Initialize members of the Driver Info structure.
            // Do not copy FAST_IO_DISPATCH structure of the driver because
            // it may disappear during the operaion which whould made 
            // VrtuleTree unsafe.
            RtlZeroMemory(TmpDriverInfo, DriverInfoSize);
            TmpDriverInfo->Size = DriverInfoSize;
            TmpDriverInfo->ObjectAddress = DriverObject;
            for (i = 0; i < IRP_MJ_MAXIMUM_FUNCTION + 1; ++i)
               TmpDriverInfo->MajorFunctions[i] = DriverObject->MajorFunction[i];

            TmpDriverInfo->NumberOfDevices = DeviceArrayLength;
            TmpDriverInfo->DevicesOffset = (ULONG_PTR)Data - (ULONG_PTR)TmpDriverInfo;
            // Copy addresses of Device Object structures of the driver's
            // devices into the record. These addresses will be copied to
            // Device Info structures later created for individual devices of
            // the driver. The addresses in the record will be replaced with
            // addresses of the corresponding Device Info structures.
            RtlCopyMemory(Data, DeviceArray, DeviceArrayLength * sizeof(PDEVICE_OBJECT));
            Data += DeviceArrayLength * sizeof(PDEVICE_OBJECT);
            // Copy name of the driver
            TmpDriverInfo->NameOffset = (ULONG_PTR)Data - (ULONG_PTR)TmpDriverInfo;
            RtlCopyMemory(Data, DriverName.Buffer, DriverName.Length);
            ((PWCHAR)Data)[DriverName.Length / sizeof(WCHAR)] = '\0';
            // Walk the Device Objects and replace them with DeviceInfo
            // records.
            pDeviceObject = (PDEVICE_OBJECT *)((PUCHAR)TmpDriverInfo + TmpDriverInfo->DevicesOffset);
            Status = STATUS_SUCCESS;
            for (i = 0; i < TmpDriverInfo->NumberOfDevices; ++i) {
               Status = _DeviceInfoSnapshotCreate(pDeviceObject[i], (PSNAPSHOT_DEVICE_INFO *)&pDeviceObject[i]);
               if (!NT_SUCCESS(Status)) {
                  LONG j = 0;

                  for (j = i - 1; j >= 0; --j)
                     _DeviceInfoSnapshotFree((PSNAPSHOT_DEVICE_INFO)pDeviceObject[j]);
               
                  break;
               }
            }
            
            if (NT_SUCCESS(Status)) {
               *DriverInfo = TmpDriverInfo;
            }

            if (!NT_SUCCESS(Status))
               ExFreePool(TmpDriverInfo);
         } else Status = STATUS_INSUFFICIENT_RESOURCES;

         _ReleaseDeviceArray(DeviceArray, DeviceArrayLength);
      }

      ExFreePool(DriverName.Buffer);
   }

   DEBUG_EXIT_FUNCTION("0x%x, *DriverInfo=0x%p", Status, *DriverInfo);
   return Status;
}

/** Frees memory allocated for Driver Info record. The routine also frees
 *  all Device Info records of devices owned by the driver in time when the
 *  Driver Info structure had been created.
 *
 *  @param DriverInfo Address of Driver Info structure to free.
 */
static VOID _DriverSnapshotFree(PSNAPSHOT_DRIVER_INFO DriverInfo)
{
   LONG i = 0;
   PSNAPSHOT_DEVICE_INFO *DeviceInfo = NULL;
   DEBUG_ENTER_FUNCTION("DriverInfo=0x%p", DriverInfo);

   DeviceInfo = (PSNAPSHOT_DEVICE_INFO *)((PUCHAR)DriverInfo + DriverInfo->DevicesOffset);
   for (i = 0; i < DriverInfo->NumberOfDevices; ++i)
      _DeviceInfoSnapshotFree(DeviceInfo[i]);

   ExFreePool(DriverInfo);

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/************************************************************************/
/* SNAPSHOT ROUTINES                                                    */
/************************************************************************/

/** Collects information about all drivers and devices present in the
 *  system and stores it inside a new snapshot.
 *
 *  @param Snapshot Address of variable that, in case of success, receives
 *  address of the new snapshot.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the
 *  operation.
 *
 *  @remark The routine inserts the snapshot to the list of snapshot. The
 *  routine is thread-safe.
 */
NTSTATUS SnapshotCreate(PVRTULETREE_KERNEL_SNAPSHOT *Snapshot)
{
   PSNAPSHOT_DRIVERLIST DriverList = NULL;
   UNICODE_STRING DriverDirectory;
   PDRIVER_OBJECT *NormalDrivers = NULL;
   SIZE_T NormalDriverCount = 0;
   PDRIVER_OBJECT *FileSystemDrivers = NULL;
   SIZE_T FileSystemDriverCount = 0;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("Snapshot=0x%p", Snapshot);

   // Collect Driver Objects from \Driver directory
   RtlInitUnicodeString(&DriverDirectory, L"\\Driver");
   Status = _GetDriversInDirectory(&DriverDirectory, &NormalDrivers, &NormalDriverCount);
   if (NT_SUCCESS(Status)) {
      // Collect Driver Objects from \FileSystem directory
      RtlInitUnicodeString(&DriverDirectory, L"\\FileSystem");
      Status = _GetDriversInDirectory(&DriverDirectory, &FileSystemDrivers, &FileSystemDriverCount);
      if (NT_SUCCESS(Status)) {
         PDRIVER_OBJECT *DriverArrays [2] = {NormalDrivers, FileSystemDrivers};
         SIZE_T ArrayLengths [2] = {NormalDriverCount, FileSystemDriverCount};

         // Create a node representing the list of drivers in the snapshot.
         // The node will contain all drivers found in \Driver and \FileSystem
         // directories. Creation of the node does not cause creation of
         // Driver Info and Device Info records.
         Status = _DriverListNodeCreate(DriverArrays, ArrayLengths, sizeof(ArrayLengths) / sizeof(SIZE_T), &DriverList);
         if (NT_SUCCESS(Status)) {
            LONG i = 0;
            PDRIVER_OBJECT *DriverObject = NULL;

            // Create Driver Info records for all drivers in the snapshot.
            // This will also cause the Device Info records to be created.
            DriverObject = (PDRIVER_OBJECT *)((PUCHAR)DriverList + DriverList->DriversOffset);
            for (i = 0; i < DriverList->NumberOfDrivers; ++i) {
               Status = _DriverSnapshotCreate(DriverObject[i], (PSNAPSHOT_DRIVER_INFO *)&DriverObject[i]);
               if (!NT_SUCCESS(Status)) {
                  LONG j = 0;

                  for (j = i - 1; j >= 0; --j)
                     _DriverSnapshotFree((PSNAPSHOT_DRIVER_INFO)DriverObject[j]);
               
                  break;
               }
            }

            if (NT_SUCCESS(Status)) {
               PVRTULETREE_KERNEL_SNAPSHOT TmpSnapshot = NULL;
               
               // Allocate snapshot record, initialize it and insert it
               // into the list of snapshots
               TmpSnapshot = (PVRTULETREE_KERNEL_SNAPSHOT)ExAllocatePool(PagedPool, DriverList->Size + sizeof(VRTULETREE_KERNEL_SNAPSHOT) - sizeof(SNAPSHOT_DRIVERLIST));
               if (TmpSnapshot != NULL) {
                  InitializeListHead(&TmpSnapshot->Entry);
                  RtlCopyMemory(&TmpSnapshot->DriverList, DriverList, DriverList->Size);
                  ExAcquireFastMutex(&SnapshotListMutex);
                  InsertTailList(&SnapshotList, &TmpSnapshot->Entry);
                  ExReleaseFastMutex(&SnapshotListMutex);
                  
                  *Snapshot = TmpSnapshot;
                  Status = STATUS_SUCCESS;
               } else Status = STATUS_INSUFFICIENT_RESOURCES;

               if (!NT_SUCCESS(Status)) {
                  PSNAPSHOT_DRIVER_INFO *DriverInfo = NULL;

                  DriverInfo = (PSNAPSHOT_DRIVER_INFO *)((PUCHAR)DriverList + DriverList->DriversOffset);
                  for (i = 0; i < DriverList->NumberOfDrivers; ++i)
                     _DriverSnapshotFree(DriverInfo[i]);
               }
            }

            ExFreePool(DriverList);
         }

         _ReleaseDriverArray(FileSystemDrivers, FileSystemDriverCount);
      }

      _ReleaseDriverArray(NormalDrivers, NormalDriverCount);
   }

   DEBUG_EXIT_FUNCTION("0x%x, *Snapshot=0x%p", Status, *Snapshot);
   return Status;
}

/** Removes given snapshot from the snapshot list and
 *  frees all memory allocated for it and its Driver Info
 *  and Device Info objects.
 */
VOID SnapshotFree(PVRTULETREE_KERNEL_SNAPSHOT Snapshot)
{
   LONG i = 0;
   PSNAPSHOT_DRIVER_INFO *DriverInfo;
   PSNAPSHOT_DRIVERLIST DriverList = NULL;
   DEBUG_ENTER_FUNCTION("Snapshot=0x%p", Snapshot);

   ExAcquireFastMutex(&SnapshotListMutex);
   RemoveEntryList(&Snapshot->Entry);
   ExReleaseFastMutex(&SnapshotListMutex);

   DriverList = &Snapshot->DriverList;
   DriverInfo = (PSNAPSHOT_DRIVER_INFO *)((PUCHAR)DriverList + DriverList->DriversOffset);
   for (i = 0; i < DriverList->NumberOfDrivers; ++i)
      // Freeing Driver Info record also frees all related Device Info records
      _DriverSnapshotFree(DriverInfo[i]);

   ExFreePool(Snapshot);

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/** Initializes global variables needed to perform snapshots of existing
 *  drivers and devices.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the
 *  operation.
 */
NTSTATUS VTreeSnapshotModuleInit(VOID)
{
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION_NO_ARGS();

   InitializeListHead(&SnapshotList);
   ExInitializeFastMutex(&SnapshotListMutex);
   Status = STATUS_SUCCESS;

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}

/** Frees all snapshots in the list. When the routine finishes, the list
 *  is clear. The routine is not thread-safe.
 */
VOID SnapshotFreeAll(VOID)
{
   PVRTULETREE_KERNEL_SNAPSHOT Tmp = NULL;
   PVRTULETREE_KERNEL_SNAPSHOT Old = NULL;
   DEBUG_ENTER_FUNCTION_NO_ARGS();

   ExAcquireFastMutex(&SnapshotListMutex);
   Tmp = CONTAINING_RECORD(SnapshotList.Flink, VRTULETREE_KERNEL_SNAPSHOT, Entry);
   while (&Tmp->Entry != &SnapshotList) {
      Old = Tmp;
      Tmp = CONTAINING_RECORD(Tmp->Entry.Flink, VRTULETREE_KERNEL_SNAPSHOT, Entry);
      SnapshotFree(Old);
   }

   ExReleaseFastMutex(&SnapshotListMutex);

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}
