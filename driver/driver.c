
#include <ntifs.h>
#include "preprocessor.h"
#include "snapshot.h"
#include "ioctls.h"
#include "driver.h"

/************************************************************************/
/*                       GLOBAL VARIABLES                               */
/************************************************************************/

/** Stores number of handles opened to the communication device */
static LONG _NumberOfHandles = 0;
/** Synchronizes execution of DriverCreateClose routine. */
static KGUARDED_MUTEX _CreateCloseMutex;

/************************************************************************/
/*                        IRP DISPATCH ROUTINES                         */
/************************************************************************/

/** Dispatches IRPs of IRP_MJ_CREATE and IRP_MJ_CLOSE type. The routine
 *  counts number of handles opened to the communication device. When the
 *  number reaches zero, all existing snapshots are freed.
 */
NTSTATUS DriverCreateClose(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
   PIO_STACK_LOCATION IrpSp = NULL;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p; Irp=0x%p", DeviceObject, Irp);

   IrpSp = IoGetCurrentIrpStackLocation(Irp);
   if (ExGetPreviousMode() == UserMode) {
      KeAcquireGuardedMutex(&_CreateCloseMutex);
      switch (IrpSp->MajorFunction) {
         case IRP_MJ_CREATE:
            _NumberOfHandles++;
            break;
         case IRP_MJ_CLOSE:
            _NumberOfHandles--;
            if (_NumberOfHandles == 0)
               SnapshotFreeAll();

            break;
      }

      KeReleaseGuardedMutex(&_CreateCloseMutex);
   }

   Status = STATUS_SUCCESS;
   Irp->IoStatus.Status = Status;
   IoCompleteRequest(Irp, IO_NO_INCREMENT);

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}


/** Services IRP_MJ_DEVICE_CONTROL requests. User mode application uses these
 *  requests to control activity of the driver.
 */
NTSTATUS DriverDeviceControl(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
   PIO_STACK_LOCATION IrpStack = NULL;
   PVOID OutBuffer = NULL;
   ULONG OutBufferLength = 0;
   PVOID InBuffer = NULL;
   ULONG InBufferLength = 0;
   ULONG ControlCode = 0;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p; Irp=0x%p", DeviceObject, Irp);

   IrpStack = IoGetCurrentIrpStackLocation(Irp);
   ControlCode = IrpStack->Parameters.DeviceIoControl.IoControlCode;
   OutBufferLength = IrpStack->Parameters.DeviceIoControl.OutputBufferLength;
   InBufferLength = IrpStack->Parameters.DeviceIoControl.InputBufferLength;
   Irp->IoStatus.Information = 0;
   OutBuffer = Irp->AssociatedIrp.SystemBuffer;
   InBuffer = Irp->AssociatedIrp.SystemBuffer;
   switch (ControlCode) {
      case IOCTL_VRTULETREE_CREATE_SNAPSHOT:
         if (OutBufferLength == sizeof(PVRTULETREE_KERNEL_SNAPSHOT)) {
            Status = SnapshotCreate((PVRTULETREE_KERNEL_SNAPSHOT *)OutBuffer);
            if (NT_SUCCESS(Status))
               Irp->IoStatus.Information = sizeof(PVRTULETREE_KERNEL_SNAPSHOT);
         } else Status = STATUS_INVALID_PARAMETER;
         break;

      case IOCTL_VRTULETREE_READ_SNAPSHOT_DRIVERLIST:
         if (InBufferLength == sizeof(PSNAPSHOT_DRIVERLIST)) {
            PVRTULETREE_KERNEL_SNAPSHOT Snapshot = *(PVRTULETREE_KERNEL_SNAPSHOT *)InBuffer;
            PSNAPSHOT_DRIVERLIST DriverList = &Snapshot->DriverList;

            if (OutBufferLength >= DriverList->Size) {
               RtlCopyMemory(OutBuffer, DriverList, DriverList->Size);
               Irp->IoStatus.Information = DriverList->Size;
               Status = STATUS_SUCCESS;
            } else Status = STATUS_BUFFER_TOO_SMALL;
         } else Status = STATUS_INVALID_PARAMETER;
         break;

      case IOCTL_VRTULETREE_READ_SNAPSHOT_DRIVERINFO:
         if (InBufferLength == sizeof(PSNAPSHOT_DRIVER_INFO)) {
            PSNAPSHOT_DRIVER_INFO DriverInfo = *(PSNAPSHOT_DRIVER_INFO *)InBuffer;

            if (OutBufferLength >= DriverInfo->Size) {
               RtlCopyMemory(OutBuffer, DriverInfo, DriverInfo->Size);
               Irp->IoStatus.Information = DriverInfo->Size;
               Status = STATUS_SUCCESS;
            } else Status = STATUS_BUFFER_TOO_SMALL;
         } else Status = STATUS_INVALID_PARAMETER;
         break;


      case IOCTL_VRTULETREE_READ_SNAPSHOT_DEVICEINFO:
         if (InBufferLength == sizeof(PSNAPSHOT_DRIVER_INFO)) {
            PSNAPSHOT_DEVICE_INFO DeviceInfo = *(PSNAPSHOT_DEVICE_INFO *)InBuffer;

            if (OutBufferLength >= DeviceInfo->Size) {
               RtlCopyMemory(OutBuffer, DeviceInfo, DeviceInfo->Size);
               Irp->IoStatus.Information = DeviceInfo->Size;
               Status = STATUS_SUCCESS;
            } else Status = STATUS_BUFFER_TOO_SMALL;
         } else Status = STATUS_INVALID_PARAMETER;
         break;

      case IOCTL_VRTULETREE_FREE_SNAPSHOT:
         if (InBufferLength == sizeof(PSNAPSHOT_DRIVERLIST)) {
            SnapshotFree(*(PVRTULETREE_KERNEL_SNAPSHOT *)InBuffer);
            Status = STATUS_SUCCESS;
         } else Status = STATUS_INVALID_PARAMETER;
         break;

      default:
         DEBUG_ERROR("Invalid device control requiest 0x%x", ControlCode);
         Status = STATUS_NOT_IMPLEMENTED;
         break;
   }  

   Irp->IoStatus.Status = Status;
   IoCompleteRequest(Irp, IO_NO_INCREMENT);

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}

/** Deletes the communication device.
 *
 *  @param DriverObject Address of Driver Object of the driver.
 */
static VOID DriverFinit(PDRIVER_OBJECT DriverObject)
{
   UNICODE_STRING uSymlink;
   DEBUG_ENTER_FUNCTION("DriverObject=0x%p", DriverObject);

   RtlInitUnicodeString(&uSymlink, DRIVER_SYMLINK);
   IoDeleteSymbolicLink(&uSymlink);
   IoDeleteDevice(DriverObject->DeviceObject);

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/** Standard DriverUnload routine. Cleans all resources used by the
 *  driver.
 */
VOID DriverUnload(PDRIVER_OBJECT DriverObject)
{
   DEBUG_ENTER_FUNCTION("DriverObject=0x%p", DriverObject);

   DriverFinit(DriverObject);
   SnapshotFreeAll();

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/** Creates a communication device for the driver. The device is then used
 *  by user mode application in order to collect snapshots of drivers and
 *  devices present in the system.
 *
 *  @param DriverObject Address of Driver Object structure, passed by the
 *  system into DriverEntry.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the
 *  operation.
 */
static NTSTATUS DriverInit(PDRIVER_OBJECT DriverObject)
{
   UNICODE_STRING uDeviceName;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DriverObject=0x%p", DriverObject);

   RtlInitUnicodeString(&uDeviceName, DRIVER_DEVICE);
   Status = IoCreateDevice(DriverObject, 0, &uDeviceName, FILE_DEVICE_UNKNOWN, 0, FALSE, &DriverObject->DeviceObject);
   if (NT_SUCCESS(Status)) {
      UNICODE_STRING uLinkName;

      RtlInitUnicodeString(&uLinkName, DRIVER_SYMLINK);
      Status = IoCreateSymbolicLink(&uLinkName, &uDeviceName);
      if (NT_SUCCESS(Status)) {
         DriverObject->DriverUnload = DriverUnload;
         DriverObject->MajorFunction[IRP_MJ_CREATE] = DriverCreateClose;
         DriverObject->MajorFunction[IRP_MJ_CLOSE] = DriverCreateClose;
         DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DriverDeviceControl;
      }

      if (!NT_SUCCESS(Status)) {
         IoDeleteDevice(DriverObject->DeviceObject);
      }
   }

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}

NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DriverObject=0x%p; RegistryPath=%S", DriverObject, RegistryPath->Buffer);

   KeInitializeGuardedMutex(&_CreateCloseMutex);
   Status = VTreeSnapshotModuleInit();
   if (NT_SUCCESS(Status)) {
      Status = DriverInit(DriverObject);
   }

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}
