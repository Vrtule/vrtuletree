
#ifndef __VRTULETREE_SNAPSHOT_H_
#define __VRTULETREE_SNAPSHOT_H_

#include <ntifs.h>

typedef struct {
   SIZE_T Size;
   ULONG_PTR NameOffset;
   PVOID ObjectAddress;
   ULONG_PTR NumberOfDevices;
   ULONG_PTR DevicesOffset;  
   PDRIVER_DISPATCH MajorFunctions [IRP_MJ_MAXIMUM_FUNCTION + 1];
} SNAPSHOT_DRIVER_INFO, *PSNAPSHOT_DRIVER_INFO;

typedef struct {
   SIZE_T Size;
   ULONG_PTR NameOffset;
   PVOID ObjectAddress;
   ULONG Flags;
   ULONG Characteristics;
   ULONG DeviceType;
   ULONG_PTR NumberOfLowerDevices;
   ULONG_PTR LowerDevicesOffset;
   ULONG_PTR NumberOfUpperDevices;
   ULONG_PTR UpperDevicesOffset;
   ULONG_PTR DisplayNameOffset;
   ULONG_PTR VendorNameOffset;
   ULONG_PTR DescriptionOffset;
   ULONG_PTR EnumeratorOffset;
   ULONG_PTR LocationOffset;
   ULONG_PTR ClassNameOffset;
   ULONG_PTR ClassGuidOffset;
   PDEVICE_OBJECT DiskDevice;
} SNAPSHOT_DEVICE_INFO, *PSNAPSHOT_DEVICE_INFO;

typedef struct {
   SIZE_T Size;
   ULONG_PTR NumberOfDrivers;
   ULONG_PTR DriversOffset;
} SNAPSHOT_DRIVERLIST, *PSNAPSHOT_DRIVERLIST;

typedef struct {
   LIST_ENTRY Entry;
   SNAPSHOT_DRIVERLIST DriverList;
}  VRTULETREE_KERNEL_SNAPSHOT, *PVRTULETREE_KERNEL_SNAPSHOT;


NTSTATUS SnapshotCreate(PVRTULETREE_KERNEL_SNAPSHOT *Snapshot);
VOID SnapshotFree(PVRTULETREE_KERNEL_SNAPSHOT Snapshot);
VOID SnapshotFreeAll(VOID);

NTSTATUS VTreeSnapshotModuleInit(VOID);


#endif
