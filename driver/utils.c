
#include <ntifs.h>
#include "preprocessor.h"
#include "utils.h"


/************************************************************************/
/*                       IMPORTS                                        */
/************************************************************************/

/** Definition of ZwQueryDirectory object. */
typedef NTSTATUS (NTAPI ZWQUERYDIRECTORYOBJECT)(
   HANDLE DirectoryHandle,
   PVOID Buffer,
   ULONG Length,
   BOOLEAN ReturnSingleEntry,
   BOOLEAN RestartScan,
   PULONG Context,
   PULONG ReturnLength);

/** Definition of ObReferenceObjectByName */
typedef NTSTATUS (NTAPI OBREFERENCEOBJECTBYNAME) (
   PUNICODE_STRING ObjectPath,
   ULONG Attributes,
   PACCESS_STATE PassedAccessState OPTIONAL,
   ACCESS_MASK DesiredAccess OPTIONAL,
   POBJECT_TYPE ObjectType,
   KPROCESSOR_MODE AccessMode,
   PVOID ParseContext OPTIONAL,
   PVOID *ObjectPtr); 


/** Buffer returned by ZwQueryDirectoryObject is full of these
    structures. */
typedef struct _OBJECT_DIRECTORY_INFORMATION {
   /** Name of the object. */
   UNICODE_STRING Name;
   /** type of the object (string form). */
   UNICODE_STRING TypeName;
} OBJECT_DIRECTORY_INFORMATION, *POBJECT_DIRECTORY_INFORMATION;


/** ZwQueryDirectoryObject import. */
__declspec(dllimport) ZWQUERYDIRECTORYOBJECT ZwQueryDirectoryObject;
/** ObReferenceObjectByName import. */
__declspec(dllimport) OBREFERENCEOBJECTBYNAME ObReferenceObjectByName;
/** IoDriverObjectType import. */
__declspec(dllimport) POBJECT_TYPE *IoDriverObjectType;


/************************************************************************/
/* HELPER ROUTINES                                                      */
/************************************************************************/

/** Retrieves a property from the registry keys of given device. The property
 *  data must be a GUID.
 *
 *  @param DeviceObject Address of Device Object which property should be queried.
 *  @param Property Property to query.
 *  @param Value Address of GUID variable that, when the routine succeeds, 
 *  receives data of the property in a form of GUID.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the operation.
 */
NTSTATUS _GetDeviceGUIDProperty(PDEVICE_OBJECT DeviceObject, DEVICE_REGISTRY_PROPERTY Property, PGUID Value)
{
   ULONG ReturnLength = 0;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p; Property=%u; Value=0x%p", DeviceObject, Property, Value);

   RtlZeroMemory(Value, sizeof(GUID));
   Status = IoGetDeviceProperty(DeviceObject, Property, sizeof(GUID), Value, &ReturnLength);
   if (Status == STATUS_INVALID_DEVICE_REQUEST ||
      Status == STATUS_OBJECT_NAME_NOT_FOUND)
      Status = STATUS_SUCCESS;

   DEBUG_EXIT_FUNCTION("0x%x", Status);
   return Status;
}

/** Retrieves a property from the registry keys of given device. The property
 *  data must be a wide-character string.
 *
 *  @param DeviceObject Address of Device Object which property should be queried.
 *  @param Property Property to query.
 *  @param Buffer Address of variable that, when the routine succeeds, 
 *  receives data of the property in a form of a wide character string.
 *  The routine allocates storage for the string. When the string is no longer needed,
 *  it must be freed by a call to ExFreePool.
 *  @param BufferLength Address of variable that, in case of success, receives.
 *  the length of buffer allocated to store the property data.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the operation.
 */
NTSTATUS _GetWCharDeviceProperty(PDEVICE_OBJECT DeviceObject, DEVICE_REGISTRY_PROPERTY Property, PWCHAR *Buffer, PULONG BufferLength)
{
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   PVOID Tmp = NULL;
   ULONG TmpSize = 64;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p; Property=%u; Buffer=0x%p; BufferLength=0x%p", DeviceObject, Property, Buffer, BufferLength);

   do {
      if (Tmp != NULL) {
         ExFreePool(Tmp);
         Tmp = NULL;
      }

      Tmp = ExAllocatePool(PagedPool, TmpSize);
      if (Tmp != NULL) {
         Status = IoGetDeviceProperty(DeviceObject, Property, TmpSize, Tmp, &TmpSize);
         if (NT_SUCCESS(Status)) {
            *BufferLength = TmpSize;
            *Buffer = Tmp;
         }
      } else Status = STATUS_INSUFFICIENT_RESOURCES;
   } while (Status == STATUS_BUFFER_TOO_SMALL);

   if (!NT_SUCCESS(Status)) {
      if (Tmp != NULL)
         ExFreePool(Tmp);

      if (Status == STATUS_INVALID_DEVICE_REQUEST || 
         Status == STATUS_OBJECT_NAME_NOT_FOUND) {
            *BufferLength = 0;
            Status = STATUS_SUCCESS;
      } else {
         *Buffer = NULL;
         *BufferLength = 0;
      }
   }

   DEBUG_EXIT_FUNCTION("0x%x, *Buffer=0x%p, *BufferLength=%u", Status, *Buffer, *BufferLength);
   return Status;
}


/** Frees resources occupied by the array of pointers to DRIVER_OBJECT
 *  structures. Every object in the array is dereferenced and the array
 *  is freed.
 *
 *  @param DriverArray Address of the array.
 *  @param DriverCount Number of elements in the array.
 */
VOID _ReleaseDriverArray(PDRIVER_OBJECT *DriverArray, SIZE_T DriverCount)
{
   LONG i = 0;
   DEBUG_ENTER_FUNCTION("DriverArray=0x%p; DriverCount=%u", DriverArray, DriverCount);

   if (DriverCount > 0) {
      for (i = 0; i < DriverCount; ++i)
         ObDereferenceObject(DriverArray[i]);

      ExFreePool(DriverArray);
   }

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/** Frees resources occupied by the array of pointers to DEVICE_OBJECT
 *  structures. Every object in the array is dereferenced and the array
 *  is freed.
 *
 *  @param DriverArray Address of the array.
 *  @param DriverCount Number of elements in the array.
 */
VOID _ReleaseDeviceArray(PDEVICE_OBJECT *DeviceArray, SIZE_T ArrayLength)
{
   LONG i = 0;
   DEBUG_ENTER_FUNCTION("DeviceArray=0x%p; ArrayLength=%u", DeviceArray, ArrayLength);

   if (ArrayLength > 0) {
      for (i = 0; i < ArrayLength; ++i)
         ObDereferenceObject(DeviceArray[i]);

      ExFreePool(DeviceArray);
   }

   DEBUG_EXIT_FUNCTION_VOID();
   return;
}

/** Retrieves name of the object via ObQueryNameString.
 *
 *  @param Object Address of object which name should be retrieved.
 *  @param Name Pointer to UNICODE_STRING structure which the routine fills
 *  with the name (of course only if the routine succeeds). The routine
 *  allocates storage for the string and store address of the storage in
 *  the Buffer member of the UNICODE_STRING structure. The caller must
 *  deallocate the storage via ExFreePool when it no longer needs the string.
 *  The storage is allocated from paged pool.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the
 *  operation. 
 *
 */
NTSTATUS _GetObjectName(PVOID Object, PUNICODE_STRING Name)
{
   ULONG oniLength = 0;
   POBJECT_NAME_INFORMATION oni = NULL;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("Object=0x%p, Name=0x%p", Object, Name);

   RtlZeroMemory(Name, sizeof(UNICODE_STRING));
   Status = ObQueryNameString(Object, oni, oniLength, &oniLength);
   while (Status == STATUS_INFO_LENGTH_MISMATCH) {
      oni = (POBJECT_NAME_INFORMATION)ExAllocatePool(NonPagedPool, oniLength);
      if (oni != NULL) {
         Status = ObQueryNameString(Object, oni, oniLength, &oniLength);
         if (NT_SUCCESS(Status))
            *Name = oni->Name;

         if (!NT_SUCCESS(Status)) {
            ExFreePool(oni);
            oni = NULL;
         }
      } else Status = STATUS_INSUFFICIENT_RESOURCES;
   }

   if (NT_SUCCESS(Status)) {
      PWCHAR Tmp = NULL;

      Tmp = (PWCHAR)ExAllocatePool(PagedPool, Name->Length + sizeof(WCHAR));
      if (Tmp != NULL) {
         RtlCopyMemory(Tmp, Name->Buffer, Name->Length);
         Tmp[Name->Length / sizeof(WCHAR)] = L'\0';
         Name->Buffer = Tmp;
         ExFreePool(oni);
      } else Status = STATUS_INSUFFICIENT_RESOURCES;
   }

   if (!NT_SUCCESS(Status)) {
      if (oni != NULL)
         ExFreePool(oni);

      RtlZeroMemory(Name, sizeof(UNICODE_STRING));
   }

   DEBUG_EXIT_FUNCTION("0x%x, *Name=%S", Status, Name->Buffer);
   return Status;
}

/** Appends object name to the name of directory, so the resulting name forms
 *  absolute path to the object. The routine adds a backslash between the 
 *  directory name and the object name.
 *
 *  @param Dest The routine fills the UNICODE_STRING structure with the 
 *  resulting name. The storage for the string is allocated from paged pool
 *  and must be deallocated by the caller when no longer needed.
 *  @param Src1 UNICODE_STRING that contains (absolute) name of the directory.
 *  @param Src2 UNICODE_STRING taht contains name of the object.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the
 *  operation.
 */
static NTSTATUS _AppendDriverNameToDirectory(PUNICODE_STRING Dest, PUNICODE_STRING Src1, PUNICODE_STRING Src2)
{
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   DEBUG_ENTER_FUNCTION("Dest=0x%p; Src1=\"%S\"; Src2=\"%S\"", Dest, Src1->Buffer, Src2->Buffer);

   Dest->Length = Src1->Length + sizeof(WCHAR) + Src2->Length;
   Dest->MaximumLength = Dest->Length;
   Dest->Buffer = (PWSTR)ExAllocatePool(PagedPool, Dest->Length + sizeof(WCHAR));
   if (Dest->Buffer != NULL) {
      RtlZeroMemory(Dest->Buffer, Dest->Length + sizeof(WCHAR));
      RtlCopyMemory(Dest->Buffer, Src1->Buffer, Src1->Length);
      Dest->Buffer[Src1->Length / sizeof(WCHAR)] = L'\\';
      RtlCopyMemory(&Dest->Buffer[(Src1->Length / sizeof(WCHAR)) + 1], Src2->Buffer, Src2->Length);
      Status = STATUS_SUCCESS;
   } else Status = STATUS_INSUFFICIENT_RESOURCES;

   DEBUG_EXIT_FUNCTION("0x%x, *Dest=%S", Status, Dest->Buffer);
   return Status;
}

/** Retrieves all drivers in given object directory in a form of
 *  pointers to their DRIVER_OBJECT structures. The routine increases the
 *  reference count of every Driver Object reported to the caller by one.
 *
 *  @param Name of the directory in absolute form.
 *  @param DriverArray Address of variable that, when the function succeeds,
 *  receives address of array of pointers to DRIVER_OBJECT structures of the
 *  drivers located in object directory given in the first argument. When
 *  the caller no longer uses the array, it should release it by call to
 *  _ReleaseDriverArray routine. If _GetDriversInDirectory routine fails, the variable is
 *  filled with NULL.
 *  @param DriverCount Address of variable that, in case the routine succeeds,
 *  is filled with the number of elements in the array returned in the second
 *  parameter. If the function fails, the variable is filled with zero.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the operation.
 */
NTSTATUS _GetDriversInDirectory(PUNICODE_STRING Directory, PDRIVER_OBJECT **DriverArray, PSIZE_T DriverCount)
{
   SIZE_T TmpDriverCount = 0;
   PDRIVER_OBJECT *TmpDriverArray = NULL;
   HANDLE DirectoryHandle;
   OBJECT_ATTRIBUTES ObjectAttributes;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   UNICODE_STRING DriverTypeStr;
   DEBUG_ENTER_FUNCTION("Directory=%S; DriverArray=0x%p; DriverCount=0x%p", Directory->Buffer, DriverArray, DriverCount);

   *DriverCount = 0;
   *DriverArray = NULL;
   // Initialize string with the name of Driver Object Type.
   RtlInitUnicodeString(&DriverTypeStr, L"Driver");
   // Open the object directory specified in the first argument.
   InitializeObjectAttributes(&ObjectAttributes, Directory, OBJ_CASE_INSENSITIVE, NULL, NULL);
   Status = ZwOpenDirectoryObject(&DirectoryHandle, DIRECTORY_QUERY, &ObjectAttributes);
   if (NT_SUCCESS(Status)) {
      ULONG QueryContext = 0;
      // Assume that no directory entry exceeds 1024 bytes in length.
      UCHAR Buffer [1024];
      POBJECT_DIRECTORY_INFORMATION DirInfo = (POBJECT_DIRECTORY_INFORMATION)&Buffer;

      // Attempt to list contents of the directory, filter everything except drivers out,
      // and add the information to the array.
      do {
         RtlZeroMemory(&Buffer, sizeof(Buffer));
         Status = ZwQueryDirectoryObject(DirectoryHandle, DirInfo, sizeof(Buffer), TRUE, FALSE, &QueryContext, NULL);
         if (NT_SUCCESS(Status)) {
            // A directory entry has been retrieved. Check whether it represents
            // a Driver Object.
            if (RtlCompareUnicodeString(&DirInfo->TypeName, &DriverTypeStr, TRUE) == 0) {
               UNICODE_STRING FullDriverName;

               // Format the full name of the driver.
               Status = _AppendDriverNameToDirectory(&FullDriverName, Directory, &DirInfo->Name);
               if (NT_SUCCESS(Status)) {
                  PDRIVER_OBJECT DriverPtr = NULL;

                  // Get the address of corresponding DIRVER_OBJECT structure and
                  // increase its reference count by one. ObReferenceObjectByName will
                  // do the job.
                  Status = ObReferenceObjectByName(&FullDriverName, OBJ_CASE_INSENSITIVE, NULL, GENERIC_READ, *IoDriverObjectType, KernelMode, NULL, (PVOID *)&DriverPtr);
                  if (NT_SUCCESS(Status)) {
                     PDRIVER_OBJECT *Tmp = NULL;

                     // Reallocate the array of Driver Objects, so one more
                     // pointer can be inserted inside. This is really 
                     // ineffective. The array should be preallocated in order
                     // to decrease numbere of reallocations.
                     Tmp = (PDRIVER_OBJECT *)ExAllocatePool(PagedPool, (TmpDriverCount + 1) * sizeof(PDRIVER_OBJECT));
                     if (Tmp != NULL) {
                        RtlCopyMemory(Tmp, TmpDriverArray, TmpDriverCount * sizeof(PDRIVER_OBJECT));
                        Tmp[TmpDriverCount] = DriverPtr;
                        if (TmpDriverArray != NULL)
                           ExFreePool(TmpDriverArray);

                        TmpDriverArray = Tmp;
                        ++TmpDriverCount;
                     } else Status = STATUS_INSUFFICIENT_RESOURCES;

                     if (Tmp == NULL)
                        ObDereferenceObject(DriverPtr);
                  }

                  ExFreePool(FullDriverName.Buffer);
               }
            }
         }
      } while (NT_SUCCESS(Status));

      if (Status == STATUS_NO_MORE_ENTRIES) {
         // The object directory has been successfully traversed.
         // Report success and return the array and number of its elements
         // in the second and the third parameters.
         *DriverCount = TmpDriverCount;
         *DriverArray = TmpDriverArray;
         Status = STATUS_SUCCESS;
      } else {
         _ReleaseDriverArray(TmpDriverArray, TmpDriverCount);
      }

      ZwClose(DirectoryHandle);
   } else {
      DEBUG_ERROR("ERROR: ZwOpenDirectoryObject: 0x%x", Status);
   }

   DEBUG_EXIT_FUNCTION("0x%x, *DriverArray=0x%p, *DriverCount=%u", Status, *DriverArray, *DriverCount);
   return Status;
}

/** Retrieves device objects placed lower or upper in the given device's stack.
 *  Reference count of every reported Device object is increased by one.
 *
 *  @param DeviceObject Device which device stack should be examined.
 *  @param Upper Determines the direction of device stack examination. If set to TRUE,
 *  the routine attempts to report upper devices. If set to FALSE, only lower devices
 *  are reported.
 *  @param DeviceArray Address of variable that, when the function succeeds,
 *  is filled address of array of pointers to DEVICE_OBJECT structures. The
 *  array is allocated from paged memory and should be freed by call to
 *  _ReleaseDeviceArray when no longer needed. When _GetLowerUpperDevices fails,
 *  the variable is filled with NULL.
 *  @param ArrayLength Address of variable that, in case of success, receives
 *  number of elements inside array reported through the third parameter. If
 *  the function fails, the variable is filled with zero.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the operation.
 */
NTSTATUS _GetLowerUpperDevices(PDEVICE_OBJECT DeviceObject, BOOLEAN Upper, PDEVICE_OBJECT **DeviceArray, PSIZE_T ArrayLength)
{
   PDEVICE_OBJECT *TmpDeviceArray = NULL;
   SIZE_T TmpArrayLength = 0;
   NTSTATUS Status = STATUS_UNSUCCESSFUL;
   PDEVICE_OBJECT TmpDeviceObject = NULL;
   PDEVICE_OBJECT OldTmpDeviceObject = NULL;
   DEBUG_ENTER_FUNCTION("DeviceObject=0x%p; Upper=%u; DeviceArray=0x%p; ArrayLength=0x%p", DeviceObject, Upper, DeviceArray, ArrayLength);

   *DeviceArray = NULL;
   *ArrayLength = 0;
   TmpDeviceObject = Upper ? IoGetAttachedDeviceReference(DeviceObject) : IoGetLowerDeviceObject(DeviceObject);

   Status = STATUS_SUCCESS;
   while (((Upper && TmpDeviceObject != DeviceObject) || (!Upper && TmpDeviceObject != NULL)) && NT_SUCCESS(Status)) {
      PDEVICE_OBJECT *Tmp = NULL;

      OldTmpDeviceObject = TmpDeviceObject;
      // Reallocate the array, so additional device can be inserted.
      // This is really ineffective method how to implement the dynamic
      // array, so it should be changed in the future.
      Tmp = (PDEVICE_OBJECT *)ExAllocatePool(PagedPool, (TmpArrayLength + 1) * sizeof(PDEVICE_OBJECT));
      if (Tmp != NULL) {
         RtlCopyMemory(Tmp, TmpDeviceArray, TmpArrayLength * sizeof(PDEVICE_OBJECT));
         Tmp[TmpArrayLength] = OldTmpDeviceObject;
         TmpArrayLength++;
         if (TmpDeviceArray != NULL)
            ExFreePool(TmpDeviceArray);

         TmpDeviceArray = Tmp;
      } else {
         ObDereferenceObject(OldTmpDeviceObject);
         Status = STATUS_INSUFFICIENT_RESOURCES;
      }

      TmpDeviceObject = IoGetLowerDeviceObject(TmpDeviceObject);
   }

   if (NT_SUCCESS(Status)) {
      if (TmpDeviceObject == DeviceObject)
         ObDereferenceObject(TmpDeviceObject);

      // The device stack has been successfully traversed. report the
      // success and retrieve the device array and number of its elements.
      *DeviceArray = TmpDeviceArray;
      *ArrayLength = TmpArrayLength;
   }

   if (!NT_SUCCESS(Status)) {
      if (TmpDeviceObject != NULL)
         ObDereferenceObject(TmpDeviceObject);

      _ReleaseDeviceArray(TmpDeviceArray, TmpArrayLength);
   }

   DEBUG_EXIT_FUNCTION("0x%x, *DeviceArray=0x%p, *ArrayLength=%u", Status, *DeviceArray, *ArrayLength);
   return Status;
}

/** Enumerates devices of given driver. The devices are returned in form of
 *  array of pointers to DEVICE_OBJECT structures. Reference count of every
 *  device object reported by the routine is increased by one.
 *
 *  @param DriverObject Driver which devices should be enumerated.
 *  @param DeviceArray Address of variable that, in case of success, receives
 *  address of array of pointer to DEVICE_OBJECT structures. The array is allocated
 *  from nonpaged memory and must be released by _ReleaseDeviceArray when no longer
 *  needed. When _EnumDriverDevices fails, the variable is filled with NULL.
 *  @param DeviceArrayLength Address of variable that, in case of success, receives
 *  number of elements in the array retrieved in the second parameter. When the
 *  function fails, the variable is filled with zero.
 *
 *  @return Returns NTSTATUS value indicating success or failure of the operation.
 */
NTSTATUS _EnumDriverDevices(PDRIVER_OBJECT DriverObject, PDEVICE_OBJECT **DeviceArray, PULONG DeviceArrayLength)
{
   ULONG TmpArrayLength = 0;
   PDEVICE_OBJECT *TmpDeviceArray = NULL;
   NTSTATUS Status = STATUS_SUCCESS;
   DEBUG_ENTER_FUNCTION("DriverObject=0x%p; DeviceArray=0x%p; DeviceArrayLength=0x%p", DriverObject, DeviceArray, DeviceArrayLength);

   do {
      Status = IoEnumerateDeviceObjectList(DriverObject, TmpDeviceArray, TmpArrayLength * sizeof(PDEVICE_OBJECT), &TmpArrayLength);
      if (Status == STATUS_BUFFER_TOO_SMALL) {
         if (TmpDeviceArray != NULL)
            ExFreePool(TmpDeviceArray);

         TmpDeviceArray = (PDEVICE_OBJECT *)ExAllocatePool(NonPagedPool, TmpArrayLength * sizeof(PDEVICE_OBJECT));
         if (TmpDeviceArray == NULL)
            Status = STATUS_INSUFFICIENT_RESOURCES;
      }
   } while (Status == STATUS_BUFFER_TOO_SMALL);

   if (NT_SUCCESS(Status)) {
      *DeviceArrayLength = TmpArrayLength;
      *DeviceArray = TmpDeviceArray;
   }

   DEBUG_EXIT_FUNCTION("0x%x, *DeviceArray=0x%p, *DeviceArrayLength=%u", *DeviceArray, *DeviceArrayLength);
   return Status;
}
